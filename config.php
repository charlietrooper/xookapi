<?php
/**
 *
 * Configuration
 *
 * Created by PhpStorm.
 * User: AppStudio
 * Date: 15/05/2015
 * Time: 08:12 PM
 */

//Strict errors
error_reporting(E_ALL);
ini_set('display_errors', 1);

//WSDL cache
ini_set("soap.wsdl_cache_enabled", "0");

//API version
define("XOOK_API_VER", "0.0.1");
define("VERSION", "0.0.1");
define("LOG_FILE", "logs/api_" . date("Y-m-d") . ".log");

//WSDL Gandhi
define("GANDHI_USER_AUTH", "g4ndh1_");
define("GANDHI_PASS_AUTH", "#864ybUr");
define("GANDHI_WSDL_PATH", "https://www.gandhi.com.mx/index.php/api/soap/?wsdl");

//Kobo services
define("KOBO_AUTHENTICATION_SERVICE", "https://services.kobobooks.com/Partners/BackEndIntegration/V2/AuthenticationService.svc?wsdl");
define("KOBO_PURCHASING_SERVICE", "https://services.kobobooks.com/Partners/BackEndIntegration/V1/PurchasingService.svc?wsdl");
define("KOBO_LIBRARY_SERVICE", "https://services.kobobooks.com/partners/backendintegration/v1/LibraryService.svc?wsdl");

//Kobo services endpoint
define("KOBO_AUTHENTICATION_SERVICE_ENPOINT", "https://services.kobobooks.com/Partners/BackEndIntegration/V2/AuthenticationService.svc?wsdl");
define("KOBO_PURCHASING_SERVICE_ENPOINT", "https://services.kobobooks.com/Partners/BackEndIntegration/V1/PurchasingService.svc?wsdl");
define("KOBO_LIBRARY_SERVICE_ENPOINT", "https://services.kobobooks.com/partners/backendintegration/v1/LibraryService.svc?wsdl");

//Catalog conection
define("CATALOGO", "https://catalog.orbile.com/");

define('X_KOBO_PARTNER', 'Xook');
define('X_KOBO_APIKEY', '3c27b3a8318a4d9899ff5398e52270a3');

//headers
define('X_API_USER_HEADER', 'x-api-user');
define('X_API_KEY_HEADER', 'x-api-key');



