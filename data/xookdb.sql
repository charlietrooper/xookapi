-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: localhost    Database: xookdb
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `affiliates`
--

DROP TABLE IF EXISTS `affiliates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliates` (
  `affiliate_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `big_logo` varchar(30) DEFAULT NULL,
  `small_logo` varchar(30) DEFAULT NULL,
  `domain_url` varchar(30) DEFAULT NULL,
  `layout` varchar(30) DEFAULT NULL,
  `configuration` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`affiliate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliates`
--

LOCK TABLES `affiliates` WRITE;
/*!40000 ALTER TABLE `affiliates` DISABLE KEYS */;
/*!40000 ALTER TABLE `affiliates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_log`
--

DROP TABLE IF EXISTS `api_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `call_me` varchar(20) DEFAULT NULL,
  `descrip` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_log`
--

LOCK TABLES `api_log` WRITE;
/*!40000 ALTER TABLE `api_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authenticate`
--

DROP TABLE IF EXISTS `authenticate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authenticate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) DEFAULT NULL,
  `user_email` varchar(20) DEFAULT NULL,
  `result` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authenticate`
--

LOCK TABLES `authenticate` WRITE;
/*!40000 ALTER TABLE `authenticate` DISABLE KEYS */;
/*!40000 ALTER TABLE `authenticate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_feed_file`
--

DROP TABLE IF EXISTS `catalog_feed_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_feed_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_path` varchar(50) NOT NULL,
  `server_path` varchar(50) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `num_ebooks` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_feed_file`
--

LOCK TABLES `catalog_feed_file` WRITE;
/*!40000 ALTER TABLE `catalog_feed_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_feed_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogueupdates`
--

DROP TABLE IF EXISTS `catalogueupdates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogueupdates` (
  `catalogue_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(70) DEFAULT NULL,
  `date_update` date DEFAULT NULL,
  PRIMARY KEY (`catalogue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogueupdates`
--

LOCK TABLES `catalogueupdates` WRITE;
/*!40000 ALTER TABLE `catalogueupdates` DISABLE KEYS */;
INSERT INTO `catalogueupdates` VALUES (1,'primer entregable','2015-06-04'),(2,'segundi entregable','2015-06-05'),(3,'tercer entregable','2015-06-06');
/*!40000 ALTER TABLE `catalogueupdates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) NOT NULL,
  `kobo_id` varchar(32) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `register_date` date DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,26,'234rfw34rw','yoni@yahoo.com','yoni','2015-06-04'),(2,26,'yyt76542ddd25','juan@outlook.com','juan','2015-06-01'),(3,27,'iiu877766ffgwt557jg','pedro@email.com','pedro','2015-06-07'),(4,28,'jjs8765rdsio907','manuel@hotmail.com','manuel','2015-06-08'),(5,28,'oiute6938uey7yg7','jorge@yahoo.com','jorge','2015-06-02');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notificationcatalogues`
--

DROP TABLE IF EXISTS `notificationcatalogues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationcatalogues` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_notification` date NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `catalogue_id` int(11) NOT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notificationcatalogues`
--

LOCK TABLES `notificationcatalogues` WRITE;
/*!40000 ALTER TABLE `notificationcatalogues` DISABLE KEYS */;
INSERT INTO `notificationcatalogues` VALUES (1,'2015-06-04','ricardo@gandhi.com',27,1),(2,'2015-06-04','franco@porrua.com',28,1),(3,'2015-06-05','ricardo@gandhi.com',27,2),(4,'2015-06-06','franco@porrua.com',28,3);
/*!40000 ALTER TABLE `notificationcatalogues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `email_support` varchar(20) DEFAULT NULL,
  `email_notification` varchar(20) DEFAULT NULL,
  `api_username` varchar(30) DEFAULT NULL,
  `api_pass` varchar(65) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  PRIMARY KEY (`partner_id`),
  UNIQUE KEY `partner_id` (`partner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (26,'gandhi','gandhi@mx.com','gandhi@mx.com','gandhi','$2a$08$JiqdkYEJHy4QMYPHqClJquOuuEFg4wXCImYNxfjU4RvN5q.WpYciW'),(27,'porrua','porrua@mx.com','porrua@mx.com','porrua','$2a$08$2iZRiuBDmK6WrMFbnhyM1.Bq6Mpqj7vWHaOkc6F5OT5F43tguwdem'),(28,'calabozo','calabozo@mx.com','calabozo@mx.com','calabozo','$2a$08$Bj4XXDzIKJOZ8a0xLsvgjeOIHq6wRVe24Gqu4y4mO/zHBZ9drlJDG');
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchases` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` varchar(20) DEFAULT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `total_products` varchar(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `total_price` varchar(20) DEFAULT NULL,
  `result` varchar(5) DEFAULT NULL,
  `purchased_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases`
--

LOCK TABLES `purchases` WRITE;
/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases_line`
--

DROP TABLE IF EXISTS `purchases_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchases_line` (
  `line_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` varchar(12) DEFAULT NULL,
  `product_id` varchar(50) DEFAULT NULL,
  `sub_price` varchar(20) DEFAULT NULL,
  `download_url` varchar(200) DEFAULT NULL,
  `content_format` varchar(20) DEFAULT NULL,
  `result` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`line_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases_line`
--

LOCK TABLES `purchases_line` WRITE;
/*!40000 ALTER TABLE `purchases_line` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchases_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin'),(2,'partner'),(3,'affiliate');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(70) NOT NULL,
  `pass` varchar(70) NOT NULL,
  `name` varchar(120) NOT NULL,
  `email` varchar(70) NOT NULL,
  `created_at` date NOT NULL,
  `active` char(1) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$2a$08$gm33BEa8VdL6iY68SOQlZebUBMubNfr9vT0zrzA95foidrqYgFulq','admin','admin@gmail.com','2015-06-17','s',1,0),(2,'ricardo','$2a$08$qwJkv6xLwO2S2Q14sF1AceZOdC2h0iS7mRveqyg2Yl/3MrkSt.jA2','ricardo','ricardo@gandhi.com','2015-06-17','s',2,27),(3,'franco','$2a$08$IC7GHAetNQ8Eie3XuwHeuOvYBFAadhYS6LpkGLP.fCwTL5Hjddare','franco','franco@porrua.com','2015-06-17','s',2,28),(4,'pedro','$2a$08$UnnPmYg8DaMUET3TwOUDmeECT2NRZqAywSSYFIzFxbQRAT.xU3WUq','pedro','pedro@porrua.com','2015-06-25','s',2,27);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-26 15:15:55
