<?php

/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 03/05/2016
 * Time: 11:16 PM
 */

/**
 * Class Generos
 */

class Generos extends \Phalcon\Mvc\Model
{
    public $id_genero;
    public $tipo_genero;
    public $genero;
}