<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 04/11/2015
 * Time: 03:23 PM
 */

class MergePartner
{


    //Funcion para llamar el webService de porrua
    public function MergePorrua($email, $pass)
    {
        $service_url = 'https://porrua.mx/webservice/AuthenticationServiceRest.php?metodo=CheckLogin&Email=' . $email . '&Password=' . rawurlencode($pass);


        $curl = curl_init($service_url);
        $curl_post_data = array(
            "Email" => $email,
            "Password" => rawurlencode($pass),
        );

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);


        if ($curl_response === FALSE) {
            $error = curl_error($curl);
            //$this->logger->log("cURL Error: $error");

           $curl_response = array('Error' => 'Fallo la conexion con el servidor');
        }

        return $curl_response;

        //curl_close($curl);


    }
/////////


    //Funcion para llamar el WebService de Gandhi
    public function MergeGhandi($email, $pass)
    {
        try
        {
            $client = new SoapClient(GANDHI_WSDL_PATH);

            $session = $client->login(GANDHI_USER_AUTH, GANDHI_PASS_AUTH);

            if ($pass == null) {
                $gandhi_response = $client->call($session, 'customer.authenticate', array($email));
            } else {
                $gandhi_response = $client->call($session, 'customer.authenticate', array($email, $pass));
            }

        }
        catch(SoapFault $e)
        {
            //$this->logger->log($e->getMessage());
            $gandhi_response = array('Error' => 'Fallo la conexion con el servidor');
        }

        return $gandhi_response;


        //$status_gandhi = $gandhi_response['success'];
        //$ref_gandhi = $gandhi_response['reference'];

    }
}