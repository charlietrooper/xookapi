<?php

/**
 * Created by PhpStorm.
 * User: carlos-silva
 * Date: 04/06/2015
 * Time: 12:50
 */

/**
 * Class Customers
 *
 */

class Customers extends \Phalcon\Mvc\Model
{
    public $customer_id;
    public $partner_id;
    public $kobo_id;
    public $email;
    public $result;
    public $register_date;
    public $partner_customer_id;
    public $orbile_customer_id;
    public $account_type;
    public $merge;

    /**
     *funcion que genera uuid
     */
    public function genUUID(){

        $db = $this->getReadConnection();
        $result = $db->query("SELECT UUID()");
        $arr = $result->fetch();
        return $arr[0];
    }


}