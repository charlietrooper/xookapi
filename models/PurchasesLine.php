<?php

/**
 * Created by PhpStorm.
 * User: rrendons
 * Date: 30-06-2015
 * Time: 17:13
 */

/**
 * Class PurchasesLine
 */

class PurchasesLine extends \Phalcon\Mvc\Model
{
    public function getSource()
    {
        return "purchases_line";
    }

    public $line_id;
    public $purchase_id;
    public $result;
    public $product_id;
    public $cogs_amount;
    public $cogs_currency_code;
    public $price_currency_code;
    public $price_discount_applied;
    public $price_before_tax;
    public $item_id;
    public $download_url;
    public $content_format;
    public $idioma;
    public $genero;
    public $is_kids;
    public $is_adult_material;
    public $cogs;
    public $titulo;
    public $isbn;
    public $genero_bic;
    public $genero_bisac;
    public $purchase_sum;
    public $purchase_book_num;
    public $account_holder_id;
    public $imprint;


    /*
     * Función para actualizar la suma de costo y numeros de libros vendidos por venta
     */
    public static function actualizaCantidadCosto($product_id, $purchase_id)
    {
        //Se define la variable global $logger para poder mandar errores y debugs al log
        global $logger;

        $purchase_line = PurchasesLine::findFirst(array(
            'product_id = :product_id: AND purchase_id = :purchase_id:',
            'bind'  => array('product_id' => $product_id, 'purchase_id' => $purchase_id)
        ));
        $purchase_line->result = 'PreOrderCanceled';
        $success_pl = $purchase_line->update();

        if($success_pl){
            $suma = PurchasesLine::sum(array(
                "column"     => "price_before_tax",
                "purchase_id = :purchase_id: AND (result = 'Success' OR result = 'PreOrder')",
                "bind" => array("purchase_id" => $purchase_id)
            ));

            $logger->log('resultado suma: ' . json_encode($suma));


            $conteo = PurchasesLine::count(array(
                "(result = 'Success' OR result = 'PreOrder') AND purchase_id = :purchase_id:",
                "bind" => array("purchase_id" => $purchase_id)
            ));

            $logger->log('resultado conteo: ' . json_encode($conteo));


            if($suma && $conteo) {
                $purchases = Purchases::findFirst($purchase_id);

                $purchases->purchase_book_num = $conteo;
                $purchases->purchase_sum = $suma;
                $success_p = $purchases->save();
                if($success_p){
                    return 'Se actualizaron los datos con éxito';
                } else {
                    return 'No se actualizó con éxito los datos de la compra';
                }
            } else {
                return 'No se ejecutaron con éxito la suma y el conteo';
            }
        } else {
            return 'No se encontró la linea del producto en purchases_line';
        }
    }
}