<?php

/**
 * Created by PhpStorm.
 * User: rrendons
 * Date: 30-06-2015
 * Time: 14:40
 */

/**
 * Class Purchases
 */

class Purchases extends \Phalcon\Mvc\Model
{
    public function getSource()
    {
        return "purchases";
    }

    public $purchase_id;
    public $partner_id;
    public $user_id;
    public $date;
    public $purchase_kobo_id;
    public $purchase_partner_id;
    public $address_city;
    public $address_country;
    public $address_state_provincy;
    public $address_zip_postal_code;
    public $payment_method;
    public $result;
    public $orbile_customer_id;
}