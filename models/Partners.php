<?php

/*use Phalcon\Mvc\Model\Validator\PresenceOf as PresenceOfValidator,
    Phalcon\Mvc\Model\Validator\Email as EmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator,
    Phalcon\Mvc\Model\Validator\StringLength as StringLengthValidator;
*/

/**
 * Class Partners
 */



class Partners extends \Phalcon\Mvc\Model
{

    public $partner_id;
    public $name;
    public $email_support;
    public $email_notification;
    public $api_username;
    public $api_pass;
    public $order_prefix;

    /**
     *
     * Utility method to authenticate and get partner details
     *
     * @param $api_user
     * @param $api_key
     */
    public static function getPartnerByAPICredentials($api_user, $api_key)
    {
        //Check if user api combination is ok

        $partner = Partners::findFirst(array("api_username=:api_username: AND api_pass = :api_pass:",
                "bind" => array("api_username" => $api_user, "api_pass" => $api_key))
        );

        return $partner;
    }

}
