<?php

class Translations
{
    public function spanish_api_translation($code)
    {
        switch($code) {
            /**
             * General Spanish Translation
             *
             */

            case "Success": $string = "Proceso Exitoso"; break;
            case "InternalError": $string = "Error Interno"; break;
            case "UserNotFound": $string = "El usuario no ha sido encontrado"; break;

            /**
             * Authentication Service Spanish Translation
             *
             */

            //Authenticate or Register

            case "DuplicateEmailCanBeMergedThroughAPI": $string = "Este correo está asociado a otro ID de usuario, puede ser combinado mediante la API"; break;
            case "DuplicateEmailCannotBeMergedThroughAPI": $string = "El email proporcionado no puede ser utilizado para comprar en el sistema, está asociado a una cuenta Kobo"; break;

            //Merge existing user

            case "UserCanNotBeMerged": $string = "El usuario no puede ser combinado"; break;
            case "AuthenticationFailed": $string = "Fallo en la autenticación"; break;

            /**
             * Library Service Spanish Translation
             *
             */

            //Get Download URLs

            case "ItemNotFound": $string = "No se encontró el artículo"; break;
            case "ItemNotInUserLibrary": $string = "No se encuentra el artículo en la librería del usuario"; break;

            /**
             * Purchasing Service Spanish Translation
             *
             */

             //Verify Purchase and Purchase

            case "ContentAlreadyOwned": $string = "Contenido ya adquirido"; break;
            case "ContentNotAllowedInGeo": $string = "Contenido no disponible para esta localización geográfica"; break;
            case "ItemsIsFreeNotMonetized": $string = "El artículo es gratuito, no puede ser monetizado"; break;
            case "ContentNotActive": $string = "Contenido no activo"; break;
            case "ContentNotFound": $string = "Contenido no encontrado"; break;
            default: $string = $code; break;
        }
        return $string;
    }

}
