<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 17/11/2015
 * Time: 05:09 PM
 */

class CallFeed extends \Phalcon\Mvc\Model
{
    /**
     * Función para separar los libros en pre-órdenes y no pre-órdenes
     * @param $products
     * @return array: 0->Pre-ordenes; 1->No Pre-ordenes
     */
    public function separarLibros($products){
        global $logger;

        //Se crea un array para meter a los libros que son pre-órdenes
        $booksPreorder = array();
        //Se crea un array para los libros que no son pre-órdenes
        $booksPartner = array();

        //Se recorren los productos para ver si hay alguno en preorder y separarlos de los que no son preordenes
        foreach ($products as $nom => $val) {
            //Obtenemos los datos del libro de la clase CallFeed
            $isPreOrder = $this->VerifyActiveRev($val->ProductId);

            if ($isPreOrder == 'true') {
                $logger->log("El libro $val->ProductId con preorder $isPreOrder");
                //Metemos el libro en pre-orden en su respectivo array
                array_push($booksPreorder, $val);

            } else {
                $logger->log("El libro $val->ProductId con preorder $isPreOrder");
                //Metemos el libro que no es pre-orden en su respectivo array
                array_push($booksPartner, $val);
            }
        }

        return array($booksPreorder, $booksPartner);
    }

    /*
     * Debe de reresar true si el producto en este momento esta en pre-orden
     * y no se puede hacer la compra directa
     */
    public function VerifyActiveRev($product_id)
    {
        global $logger;

        //Se lleva a cabo la conexión cURL
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, CATALOGO."/books/getBook/rev/$product_id");
        curl_setopt($curl, CURLOPT_TIMEOUT, 80);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        //Se obtiene el resultado de la conexión cURL
        $curl_response = curl_exec($curl);

        if($curl_response === false)
        {
            $error = 'Curl error: ' . curl_error($curl);
            $logger->error($error);
            $esPreOrden = 'false';
        }
        else
        {
            //decodificación del resultado
            $res_catalog = json_decode($curl_response, true);

            //Se obtiene la fecha actual
            $hoy = strtotime(date('Y-m-d H:i:s'));

            //Se obtiene el valor de la fecha de available(Lanzamiento para venta del libro)
            $available = strtotime($res_catalog['activeRevision']['available']['from']['@value']);

            if($available <= $hoy){
                $esPosiblePreOrden = false;
            }
            else{
                if(isset($res_catalog['activeRevision']['availableForPreorder']['from']['@value'])){
                    $esPosiblePreOrden = true;
                }
                else{
                    $esPosiblePreOrden = false;
                }
            }
            //si se cuplen las condiciones anteriores, se pasa al siguiente filtro, sino el libro es falsa pre orden
            if($esPosiblePreOrden == true)
            {
                unset($esPosiblePreOrden);

                //Se obtienen los datos de las fechas y del preorden
                $from = strtotime($res_catalog['activeRevision']['prices']['price']['@attributes']['from']);
                $preOrden = $res_catalog['activeRevision']['prices']['price']['@attributes']['isPreOrder'];

                //Se verifica si en la colección de precios que esta en raíz de price, coincide con el rango de fechas y si es preorder en caso de que coincida
                $esPosiblePreOrden = ((isset($res_catalog['activeRevision']['prices']['price']['@attributes']['to']) ? ($from <= $hoy && $hoy <= strtotime($res_catalog['activeRevision']['prices']['price']['@attributes']['to'])) : $from <= $hoy) ? ($preOrden == 'true') : false) ? true : false;

                //Si se cumplen las condiciones anteriores, el libro es pre orden, sino, se revizarán las siguientes colecciones de precios en caso de que existan
                if($esPosiblePreOrden == true)
                {
                    $esPreOrden = 'true';
                }
                else
                {
                    $precio = $res_catalog['activeRevision']['prices']['price'];
                    foreach($precio as $p => $val)
                    {
                        if (isset($val['@attributes']['currency'])){
                            //Se definen las fechas y el preorden
                            $From = strtotime($val['@attributes']['from']);
                            $PreOrden = $val['@attributes']['isPreOrder'];

                            //Se verifica si la coleccion de precios de los arrays 0,1...n, coincide con el rango de fechas y si es preorder en caso de que coincida
                            $PosiblePreOrden = ((isset($val['@attributes']['to']) ? ($From <= $hoy && $hoy <= strtotime($val['@attributes']['to'])) : $From <= $hoy) ? ($PreOrden == 'true') : false) ? true : false;

                            //Si se cumplen las condiciones anteriores, el libro es preorden y se rompe el ciclo
                            if($PosiblePreOrden == true)
                            {
                                $esPreOrden = 'true';
                                break;
                            }
                        }
                    }
                    //Si no se cumplió ninguno de las condiciones del foreach, entonces el libro será falsa preorden
                    if(!isset($PosiblePreOrden) || $PosiblePreOrden == false){
                        $esPreOrden = 'false';
                    }
                }
            } else
            {
                $esPreOrden = 'false';
            }
        }
        $logger->error('Respuesta: ' . $esPreOrden);
        return $esPreOrden;
    }

    /*
     * Devuelve los detalles de los libros en pre-orden
     */
    public function PreOrder($product_id)
    {

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, CATALOGO."/books/getBook/rev/$product_id");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $catalog = json_decode($response, true);

        $title = $catalog['activeRevision']['title'];

        $isbn = $catalog['isbn'];

        $act_rev = $catalog['activeRevision']['@attributes']['id'];

        $date = $catalog['activeRevision']['available']['from']['@value'];

        $res = array('title'=>$title, 'isbn'=>$isbn, 'actRev'=>$act_rev, 'date'=>$date);

        return $res;
    }


    /*
     * Función para obtener el idioma y el género de un libro
     */
    public function fetchBookMetadata($id_libro)
    {
        set_time_limit(0);
        //Se define la variable global $logger para poder mandar errores y debugs al log
        global $logger;

        //Se define la varibale que contendrá la respuesta de los datos obtenidos
        $respuesta = array();

        //Se realiza la conexión cURL
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, CATALOGO."/books/getBook/rev/$id_libro");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        $catalog = json_decode($response, true);
        //Se cacha algún error que pueda haber con la conexión cURL
        if($response === false)
        {
            $error = 'Curl error: ' . curl_error($curl);
            $logger->error($error);
            $respuesta = 'error';
        }
        //Obtenemos los datos necesarios:
        elseif($catalog['activeRevision']['@attributes']['id'] == null){
            $respuesta = 'libro no encontrado';
            $logger->error($respuesta);
        }
        else        {
            //$logger->log(json_encode($respuesta));
            //SE OBTIENEN LOS VALORES DE GÉNERO

            $respuesta['idioma'] = $catalog['activeRevision']['language'];

            if(!isset($catalog['activeRevision']['categories']['category'])){
                $logger->log('La categoría no está definida');
                $tipo_genero1 = null;
                $genero1 = null;
                $tipo_genero2 = null;
                $genero2 = null;
            }
            elseif(isset($catalog['activeRevision']['categories']['category']['@attributes'])){
                $tipo_genero1 = $catalog['activeRevision']['categories']['category']['@attributes']['system'];
                $genero1 = $catalog['activeRevision']['categories']['category']['@value'];
            }
            else{
                $tipo_genero1 = $catalog['activeRevision']['categories']['category']['0']['@attributes']['system'];
                $genero1 = $catalog['activeRevision']['categories']['category']['0']['@value'];
                foreach($catalog['activeRevision']['categories']['category'] as $gen){
                    if($gen['@attributes']['system'] != $tipo_genero1){
                        $tipo_genero2 = $gen['@attributes']['system'];
                        $genero2 = $gen['@value'];
                        break;
                    }
                }
            }

            //SE OBTIENE EL VALOR DE COGS

            //Se obtiene la fecha actual
            $hoy = strtotime(date('Y-m-d H:i:s'));
            //Se obtienen los datos de las fechas y del preorden
            $from = strtotime($catalog['activeRevision']['prices']['price']['@attributes']['from']);

            //Se verifica si en la colección de precios que esta en raíz de price, coincide con el rango de fechas y si es preorder en caso de que coincida
            $rangofecha = (isset($catalog['activeRevision']['prices']['price']['@attributes']['to']) ? ($from <= $hoy && $hoy <= strtotime($catalog['activeRevision']['prices']['price']['@attributes']['to'])) : $from <= $hoy) ? true : false;

            //Si se cumplen las condiciones anteriores, el libro es pre orden, sino, se revizarán las siguientes colecciones de precios en caso de que existan
            if($rangofecha == true)
            {
                $cogs = $catalog['activeRevision']['prices']['price']['cogs'];
            }
            else {
                $precio = $catalog['activeRevision']['prices']['price'];
                foreach ($precio as $p => $val) {
                    if (isset($val['@attributes']['currency'])) {
                        //Se define la fecha
                        $From = strtotime($val['@attributes']['from']);

                        //Se verifica si la coleccion de precios de los arrays 0,1...n, coincide con el rango de fechas
                        $RangoFecha = (isset($val['@attributes']['to']) ? ($From <= $hoy && $hoy <= strtotime($val['@attributes']['to'])) : $From <= $hoy) ? true : false;

                        //Si se cumple la condicion anteriorse toma el valor de cogs
                        if ($RangoFecha == true) {
                            $cogs = $val['cogs'];
                            break;
                        }
                    }
                }
                if(!isset($cogs)){
                    $cogs = 'No se encontró';
                }
            }

            //SE OBTIENEN LOS VALORES DE TITULO, ISBN, ISADULTMATERIAL, ISKIDS
            $titulo = $catalog['activeRevision']['title'];
            $isbn = $catalog['isbn'];
            $isAdultMaterial = $catalog['activeRevision']['isAdultMaterial'];
            $isKids = $catalog['activeRevision']['isKids'];
//-------
            $accountHolderId=$catalog['accountHolderId'];
            $imprint = $catalog['activeRevision']['imprint'];

            $respuesta['Generos'] = array();
            $respuesta['cogs'] = $cogs;
            $respuesta['isAdultMaterial'] = $isAdultMaterial;
            $respuesta['isKids'] = $isKids;
            $respuesta['titulo'] = $titulo;
            $respuesta['isbn'] = $isbn;
//-------
            $respuesta['accountHolderId'] = $accountHolderId;
            $respuesta['imprint'] = $imprint;


            //$logger->log('tipo género1: ' . $tipo_genero1 . 'genero1: ' . $genero1);

            if(isset($genero2)){
                $gen1 = array(
                    'tipo_genero'   => $tipo_genero1,
                    'genero'        => $genero1
                );

                $gen2 = array(
                    'tipo_genero'   => $tipo_genero2,
                    'genero'        => $genero2
                );
                array_push($respuesta['Generos'],$gen1);
                array_push($respuesta['Generos'],$gen2);

                //$logger->log('tipo género2: ' . $tipo_genero2 . 'genero2: ' . $genero2);
            }
            else{
                $gen1 = array(
                    'tipo_genero'   => $tipo_genero1,
                    'genero'        => $genero1
                );

                $gen2 = array(
                    'tipo_genero'   => null,
                    'genero'        => null
                );
                array_push($respuesta['Generos'],$gen1);
                array_push($respuesta['Generos'],$gen2);
            }

            //$logger->log('Respuesta: ' . json_encode($respuesta));
        }
        return $respuesta;
    }

    /*
     * Función para obtener el idioma y el género de un libro al momento de registrar una compra
     */
    public function ObtenerDatosLibro($id_libro)
    {
        //Se define la variable global $logger para poder mandar errores y debugs al log
        global $logger;


        $datos = $this->fetchBookMetadata($id_libro);

        if($datos == 'error' || $datos == 'libro no encontrado'){
            $logger->error('hubo un error en la conexión, o no se encontró el libro con el id: ' . $id_libro);
            $datos = 'error';
        }
        else {
            if($datos['Generos'][1]['tipo_genero'] != null || $datos['Generos'][0]['tipo_genero'] != null){
                if ($datos['Generos'][1]['tipo_genero'] != null) {
                    if ($datos['Generos'][0]['tipo_genero'] == 'BIC') {
                        $tipo_genero = $datos['Generos'][0]['tipo_genero'];
                        $id_genero = substr($datos['Generos'][0]['genero'], 0, 2);
                    } elseif ($datos['Generos'][1]['tipo_genero'] == 'BIC') {
                        $tipo_genero = $datos['Generos'][1]['tipo_genero'];
                        $id_genero = substr($datos['Generos'][1]['genero'], 0, 2);
                    } elseif ($datos['Generos'][0]['tipo_genero'] == 'BISAC') {
                        $tipo_genero = $datos['Generos'][0]['tipo_genero'];
                        $id_genero = substr($datos['Generos'][0]['genero'], 0, 3);
                    } elseif ($datos['Generos'][1]['tipo_genero'] == 'BISAC') {
                        $tipo_genero = $datos['Generos'][1]['tipo_genero'];
                        $id_genero = substr($datos['Generos'][1]['genero'], 0, 3);
                    } else {
                        $datos['genero'] = 'No disponible';
                    }
                } else {
                    if ($datos['Generos'][0]['tipo_genero'] == 'BIC') {
                        $tipo_genero = $datos['Generos'][0]['tipo_genero'];
                        $id_genero = substr($datos['Generos'][0]['genero'], 0, 2);
                    } elseif ($datos['Generos'][0]['tipo_genero'] == 'BISAC') {
                        $tipo_genero = $datos['Generos'][0]['tipo_genero'];
                        $id_genero = substr($datos['Generos'][0]['genero'], 0, 3);
                    } else {
                        $datos['genero'] = 'No disponible';
                    }
                }

                $generos = Generos::findFirst(array(
                    "id_genero = :id_genero: AND tipo_genero = :tipo_genero:",
                    "bind" => array("id_genero" => $id_genero, "tipo_genero" => $tipo_genero)
                ));

                if($generos){
                    $datos['genero'] = $generos->genero;
                }
                else{
                    $datos['genero'] = 'No disponible';
                }
            }
            else{
                $datos['genero'] = 'No disponible';
            }


            if ($datos['Generos'][0]['tipo_genero'] == 'BIC') {

                $g_bic = $datos['Generos'][0]['genero'];
                if ($datos['Generos'][1]['tipo_genero'] != null && $datos['Generos'][1]['tipo_genero'] == 'BISAC') {
                    $g_bisac = $datos['Generos'][1]['genero'];
                } else {
                    $g_bisac = null;
                }
            } elseif ($datos['Generos'][0]['tipo_genero'] == 'BISAC') {
                $g_bisac = $datos['Generos'][0]['genero'];
                if ($datos['Generos'][1]['tipo_genero'] != null && $datos['Generos'][1]['tipo_genero'] == 'BIC') {
                    $g_bic = $datos['Generos'][1]['genero'];
                } else {
                    $g_bic = null;
                }
            }
            else{
                $g_bisac = null;
                $g_bic = null;
            }
            unset($datos['Generos']);
            $datos['Generos'] = array('BIC' => $g_bic, 'BISAC' => $g_bisac);
        }
        return $datos;
    }
}
