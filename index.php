<?php
/****************
 *  Includes
 ****************/
require("lib/xml2json/xml2json.php");

use Phalcon\Db\Adapter\Pdo\Mysql as MysqlAdapter;
use Phalcon\Mvc\Micro;


//Config
include("config.php");


$loader = new \Phalcon\Loader();

$loader->registerDirs(array(
    __DIR__ . '/models/'
))->register();


//Bootstrap app
$app = new Micro();


$app['db'] = function () {
    return new MysqlAdapter(array(
        "host" => "localhost",
        "username" => "root",
        "password" => "",
        "dbname" => "xookdb",
        "charset" => "utf8", 
        "options" => array( PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8' )
    ));
};


//Set logger
$logger = new \Phalcon\Logger\Adapter\File(LOG_FILE);

//Set error handler
set_error_handler(function ($errno, $errstr, $errfile, $errline) use ($app, $logger) {
    $logger->error("$errno, $errstr, $errfile, $errline");
});

//Set fatal error logging
register_shutdown_function(function () use ($logger) {
    $error = error_get_last();
    if(isset($error['type'])){
        $logger->error("{$error['type']} {$error['message']} {$error['file']} {$error['line']}");
    }
});


/**
 * Default routes
 */

/**
 * Set handlers for top level Routes
 */

$app->get('/users/find', function () use ($logger) {

    foreach (Users::find() as $user) {
        echo $user->user_id, '<br>';
    }

});


$app->get('/', function () use ($app) {

    $json = array(
        "version" => XOOK_API_VER,
        "routes" => array()
    );


    foreach ($app->router->getRoutes() as $route) {
        $json["routes"][] = $route->getPattern();
    }
    echo "<pre>";
    echo json_encode($json, JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES);
    echo "</pre>";
});

$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo json_encode(array("error" => "Not Found"));

});


//Before middleware
$app->before(function () use ($app, $logger) {
    $logger->log("{$app->request->getScheme()} {$app->request->getHttpHost()} {$app->request->getMethod()} {$app->request->get("_url")}");

    //Only / route is accesible with no api key
    if ($app->router->getMatchedRoute()->getPattern() == "/") {
        return;
    }

    //Check credentials
    $api_user = $app->request->getHeader(X_API_USER_HEADER);
    $api_key = $app->request->getHeader(X_API_KEY_HEADER);

    if (!$api_user) {
        $app->response->setStatusCode(400, "Bad Request")->send();
        echo json_encode(array("error" => "api_user not set"));
        die();
    }

    if (!$api_key) {
        $app->response->setStatusCode(400, "Bad Request")->send();
        echo json_encode(array("error" => "api_key not set"));
        die();
    }

    //Check if user api combination is ok
    $partner = Partners::getPartnerByAPICredentials($api_user, $api_key);

    if (!$partner) {
        $app->response->setStatusCode(401, "Unauthorized")->send();
        echo json_encode(array("error" => "Bad user/key combination"));
        die();
    }


});


/**
 * Route handlers
 */
include("handlers/weather.php");
include("handlers/authservice.php");
include("handlers/authentication_service.php");
include("handlers/purchasing_service.php");
include("handlers/library_service.php");
include("handlers/pre_order_service.php");
include("handlers/sales.php");


//Handle request
$app->handle();
