<?php
/**
 * Created by PhpStorm.

 */


/**
 * Funcion que permite a un partner crear una cuenta kobo o loguear a un usuario si ya existe
 */


$app->get('/authservice/authenticate_or_register/{email}/{partner_user_id}', function ($email, $partner_user_id) use ($app, $logger) {


    //Si llega a este punto, esta instruccion te trae la info del partner
    // Ya no es necesario el partner_id en la especificacion de la funcion

    //Local type hint to get autocomplete
    /* @var $partner Partners */
    $partner = Partners::getPartnerByAPICredentials($app->request->getHeader(X_API_USER_HEADER), $app->request->getHeader(X_API_KEY_HEADER));
    //$partner = Partners::getPartnerByAPICredentials($app->request->getHeader('gandhi'), $app->request->getHeader('$08$JiqdkYEJHy4QMYPHqClJquOuuEFg4wXCImYNxfjU4RvN5q.WpYciW'));
    $partner_id = $partner->partner_id;

    $options = array(
        'trace' => 1,
        'location' => KOBO_AUTHENTICATION_SERVICE_ENPOINT,
        'stream_context' => stream_context_create(array('http' => array('header' => "x-kobo-Partner: " . X_KOBO_PARTNER . "\r\n" .
            "x-kobo-APIKey: " . X_KOBO_APIKEY . "")))
    );

    $client = new SoapClient(KOBO_AUTHENTICATION_SERVICE, $options);

    try {

        //Get customer
        //se revisa si el cliente ya existe en la bd
        $customer = Customers::findFirst(array(
            "partner_id=:partner_id: AND email = :email:",
            "bind" => array("partner_id" => $partner_id, "email" => $email))
        );

        //No fue encontrado, lo insertamos
        if (!$customer) {

            $val_user_mail = Customers::findFirst(array("email=:email:",'bind' => array("email" => $email)));

            if(!$val_user_mail) {


                $customer = new Customers();
                $customer->partner_id = $partner_id;
                $customer->email = $email;
                $customer->register_date = date("Y-m-d H:i:s");
                $customer->partner_customer_id = $partner_user_id;
                $customer->orbile_customer_id = $customer->genUUID();
                $customer->account_type = 'P';
                $customer->save();
            }
            else{
                $uuid = $val_user_mail->orbile_customer_id;

                $customer = new Customers();
                $customer->partner_id = $partner_id;
                $customer->email = $email;
                $customer->register_date = date("Y-m-d H:i:s");
                $customer->partner_customer_id = $partner_user_id;
                $customer->orbile_customer_id = $uuid;
                $customer->account_type = 'S';
                $customer->save();
            }
        }else{

            //si ya existia se actualizan solo el partner user id
            $customer->partner_customer_id = $partner_user_id;
            $customer->save();

        }

        $response = $client->AuthenticateOrRegister(array(
            'request' => array(
                "PartnerId" => $partner_id,
                "Email" => $email,
                "PartnerUserId" => $customer->orbile_customer_id
            )
        ));

        //Almacenar kobo_id que nos regresa
        if (isset($response->AuthenticateOrRegisterResult->KoboUserId)) {
            //Customer ya debe estar creado ya sea por que se encontro con findFirst o por que fue creado
            $customer->kobo_id = $response->AuthenticateOrRegisterResult->KoboUserId;
            $customer->result = $response->AuthenticateOrRegisterResult->Result;

            $customer->update();
        } else {
            $logger->error("Kobo Id vacio para {$customer->email}");
        }

        //traduccion de result de kobo
        $translation = new Translations();

        //Guardando traduccion en resultado_translation
        $translationSpanish = $translation->spanish_api_translation($response->AuthenticateOrRegisterResult->Result);

        //Creando array que tendra el mensaje de la traduccion
        $es_message = array('es' => $translationSpanish);

        //Anidando la traduccion al response de kobo
        $response->AuthenticateOrRegisterResult->Message = $es_message;

        //Enviar respuesta
        $json = json_encode($response->AuthenticateOrRegisterResult);

        //set content type
        $app->response->setContentType('application/json', 'UTF-8');

        //echo response
        echo $json;
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());


    } catch (SoapFault $e) {
        $app->response->setStatusCode(401, "Bad Request")->send();
        echo json_encode(array("error" => $e->getMessage()));

        $logger->log($client->__getLastRequestHeaders());
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());
    }
});


/**
 * Funcion que permite a un partner agregar las credenciales del partner a un usuario de Kobo existente
 */
$app->post('/authservice/merge_existing_user/{email}/{partner_user_id}', function ($email, $partner_user_id) use ($app, $logger) {

//recibimos el password por post
    $rawData = file_get_contents("php://input");


    if ($rawData == null) {
        $app->response->setStatusCode(401, "Bad Request")->send();
        echo json_encode(array("error" => "POST body null"));
        die();
    } else {
            $input = json_decode($rawData);

            //Require Password field
            if(!isset($input->Password)){
                $app->response->setStatusCode(401, "Bad Request")->send();
                echo json_encode(array("error" => "Password field missing"));
                die();
            }

            //Fetch password
            $password = $input->Password;
        }


    $options = array(
        'trace' => 1,
        'location' => KOBO_AUTHENTICATION_SERVICE_ENPOINT,
        'stream_context' => stream_context_create(array('http' => array('header' => "x-kobo-Partner: " . X_KOBO_PARTNER . "\r\n" .
            "x-kobo-APIKey: " . X_KOBO_APIKEY . "")))
    );

    $client = new SoapClient(KOBO_AUTHENTICATION_SERVICE, $options);

    try {

        $response = $client->MergeExistingUser(array(
            'request' => array(
                "Email" => $email,
                "PartnerUserId" => $partner_user_id,
                "Password" => $password
            )
        ));

        //traduccion de result de kobo
        $translation = new Translations();

        //Guardando traduccion en resultado_translation
        $translationSpanish = $translation->spanish_api_translation($response->MergeExistingUserResult->Result);

        //Creando array que tendra el mensaje de la traduccion
        $es_message = array('es' => $translationSpanish);

        //Anidando la traduccion a Message en result de kobo
        $response->MergeExistingUserResult->Message = $es_message;

        $json = json_encode($response->MergeExistingUserResult);
        //set content type
        $app->response->setContentType('application/json', 'UTF-8');
        //echo response
        echo $json;
        
        //Log response details
        $logger->log($client->__getLastRequestHeaders());
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());

    } catch (SoapFault $e) {
        $app->response->setStatusCode(401, "Bad Request")->send();
        echo json_encode(array("error" => $e->getMessage()));

        $logger->log($e->getMessage());
        $logger->log($client->__getLastRequestHeaders());
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());
    }
});


/**
 *
 * Funcion que permitef  hacer merge de una cuenta primaria y una secundaria
 *
 */

$app->post('/authservice/merge', function () use ($app, $logger) {


    $rawData = file_get_contents("php://input");

    //Analizamos si los valores resividos estan vacios
    if($rawData == null){

        //Enviamos el error en caso de que lavariable este vacia
        $app->response->setStatusCode(401, "Bad Request RawDAta")->send();
        echo json_encode(array("error" => "POST body null"));

        die();
    }
    else {
        $input = json_decode($rawData);

        if ($input == null) {

            $logger->error("JSON DECODE FAILED");
            $app->response->setStatusCode(401, "Bad Request input")->send();
            echo json_encode(array("error" => "JSON DECODE FAILED"));
            die();

        }

        //////////////

        //MAndamos a traer la clase Merge
        $Merge = new MergePartner();

        //Preguntamos si existe la primera cuenta en Porrua
        $count_porrua_p = $Merge->MergePorrua($input->email_p, $input->password_p);

        //$count_gandhi_p = $Merge->MergeGhandi($input->email_p, $input->password_p);
        //echo json_encode($count_gandhi_p);

        if(is_array($count_porrua_p))
        {
            //Si hubo algun error con la conexion al webSErvice de porrua
            echo json_encode($count_porrua_p);
        }
        else
        {

            $res_count_porrua_p = json_decode($count_porrua_p, true);

            $status_porrua = $res_count_porrua_p['Status'];
            //echo $res_count_porrua_p;

            //echo $count_porrua_p;


            $logger->log('apunto de entrar a la validacion');
            //Si el Status con la primera cuenta de Porrua es 1 entonces la cuenta no esta en porrua
            if($status_porrua == 1)
            {
                $logger->log('primer validacion');

                //Verificamos con la segunda cuenta y ver si existe en porrua
                $count_porrua_s = $Merge->MergePorrua($input->email_s, $input->password_s);

                if(is_array($count_porrua_s))
                {
                    //Si hubo un error con el webService de porrua
                    echo json_encode($count_porrua_s);
                }
                else
                {
                    $res_count_porrua_s = json_decode($count_porrua_s, true);

                    //Si el Status con la segunda cuenta es 0 entonces existe en porrua y pasamos a buscar la otra en gandhi
                    if($res_count_porrua_s['Status'] == 0)
                    {
                        $logger->log('segunda validacion');
                        //Buscamos la primera cuenta en Gandhi

                        $count_gandhi_p = $Merge->MergeGhandi($input->email_p, $input->password_p);

                        if(is_array($count_gandhi_p))
                        {
                            //Si hubo algun error con el WebService gandhi
                            echo json_encode($count_gandhi_p);
                        }
                        else
                        {
                            //Si el success es 1 entonces existe en gandhi
                            if($count_gandhi_p['success'] == 1)
                            {
                                //Buscamos cuentas y hacemos Merge en nuestra BD

                                //Buscamos en customers con la cuenta primaria
                                $customer_p = Customers::findFirst(array(
                                    'email = :email_p:',
                                    'bind' => array('email_p' => $input->email_p)
                                ));

                                //Buscamos en customers la cuenta secundaria
                                $customer_s = Customers::findFirst(array(
                                    'email = :email_s:',
                                    'bind' => array('email_s' => $input->email_s)
                                ));

                                //Emparejamos ambas cuentas con el mismo orbile_id de la primaria
                                $customer_s->orbile_customer_id = $customer_p->orbile_customer_id;

                                //CAmbiamos el acount type de las cuentas especificando cual es primaria y cual secundaria
                                $customer_p->account->type = 'P';
                                $customer_s->account->type = 'S';

                                //Guardamos los datos
                                $customer_s->save();
                                $customer_p->save();

                                /////
                                //enviamos respuesta de exito al form merge
                                echo json_encode(array('Exito' => 'Las cuentas se han emparejado'));
                            }
                            elseif($count_gandhi_p['success'] == 0)
                            {
                                //Se enviara algun error al Form de Merge en dado caso que ninguna cuenta exista
                                echo json_encode(array('Error' => 'Una de las cuentas no existe o no esta registrada'));
                            }

                        }

                    }

                    //Si el estatus envia 1 quiere decir que no existe en porrua
                    elseif($res_count_porrua_s['Status'] == 1)
                    {
                        $logger->log('hasta aqui entra');
                        //Se envia error al form merge en caso que alguna cuenta no exista
                        echo json_encode(array('Error' => 'Una de las cuentas no existe o no esta registrada'));
                    }

                }

            }
            //Su el Status con la primera cuenta es 0 la cuenta existe en porrua
            elseif($status_porrua == 0)
            {
                //Buscamos en gandhi con la segunda cuenta envia de form merge
                $count_gandhi_s = $Merge->MergeGhandi($input->email_s, $input->password_s);

                if(is_array($count_gandhi_s))
                {
                    //Si hubo algun error con la conexion del webService de gandhi
                    echo json_encode($count_gandhi_s);
                }
                else
                {
                    //Si el Success de gandhi nos dio 1 entonces existe en gandhi
                    if($count_gandhi_s['success'] == 1)
                    {
                        //Buscamos cuentas y hacemos MErge en nuestra BD

                        //Buscamos la cuenta primaria
                        $customer_p = Customers::findFirst(array(
                            'email = :email_p:',
                            'bind' => array('email_p' => $input->email_p)
                        ));

                        //Buscamos la cuenta secundaria
                        $customer_s = Customers::findFirst(array(
                            'email = :email_s:',
                            'bind' => array('email_s' => $input->email_s)
                        ));

                        //Emparejamos ambas cuentas con el mismo orbile_id de la primaria
                        $customer_s->orbile_customer_id = $customer_p->orbile_customer_id;

                        //CAmbiamos el acount type de las cuentas especificando cual es primaria y cual secundaria
                        $customer_p->account->type = 'P';
                        $customer_s->account->type = 'S';

                        //Guardamos los datos
                        $customer_s->save();
                        $customer_p->save();

                        //enviamos respuesta de exito al form merge
                        echo json_encode(array('Exito' => 'Las cuentas se han emparejado'));

                    }
                    elseif($count_gandhi_s['success'] == 0)
                    {
                        echo json_encode(array('Error' => 'Una de las cuentas no existe o no esta registrada'));
                    }
                }


            }
        }
    }

    //echo json_encode(array("error" => "No se puede realizar merge"));

});




