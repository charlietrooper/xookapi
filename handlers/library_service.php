<?php
/**
 * Created by PhpStorm.

 */

/**
 * Funcion que regresa la lista de contenido adquirido para un usuario determinado
 */
$app->get('/library_service/get_library/{items_per_page}/{page}/{user_id}', function ($items_per_page, $page, $user_id) use ($app, $logger) {

    $options = array(
        'trace' => 1,
        'location' => KOBO_LIBRARY_SERVICE_ENPOINT,
        'stream_context' => stream_context_create(array('http' => array('header' => "x-kobo-Partner: " . X_KOBO_PARTNER . "\r\n" .
            "x-kobo-APIKey: " . X_KOBO_APIKEY . "")))
    );

    $client = new SoapClient(KOBO_LIBRARY_SERVICE, $options);

    try {

        $response = $client->GetLibrary(array(
            'request' => array(
                "ItemsPerPage" => $items_per_page,
                "Page" => $page,
                "UserId" => $user_id
            )
        ));

        //handle empty library case
        if(!isset($response->GetLibraryResult->Content->Items->LibraryInfo)){
            //set to empty array to avoid warnings
            $response->GetLibraryResult->Content->Items->LibraryInfo = array();
        }

        if(!is_array($response->GetLibraryResult->Content->Items->LibraryInfo))
        {
            $response->GetLibraryResult->Content->Items->LibraryInfo = array($response->GetLibraryResult->Content->Items->LibraryInfo);
        }

        //traduccion de result de kobo
        $translation = new Translations();

        //Guardando traduccion en resultado_translation
        $translationSpanish = $translation->spanish_api_translation($response->GetLibraryResult->Result);

        //Creando array que tendra el mensaje de la traduccion
        $es_message = array('es' => $translationSpanish);

        //Agregando la traduccion al result de kobo
        $response->GetLibraryResult->Message = $es_message;

        $json = json_encode($response->GetLibraryResult);


        //set content type
        $app->response->setContentType('application/json', 'UTF-8');
        //echo response
        echo $json;
        $logger->log($client->__getLastRequest());
        $logger->log("Se omite respuesta, revisar en Xookweb los detalles de la libreria");
    } catch (SoapFault $e) {
        $logger->log($e->getMessage());
        $logger->log($client->__getLastRequestHeaders());
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());
    }


});


/**
 * Funcion que remueve un producto de un usuario determinado.
 */

$app->get('/library_service/remove_from_library/{product_id}/{user_id}', function ($product_id, $user_id) use ($app, $logger) {

    $options = array(
        'trace' => 1,
        'location' => KOBO_LIBRARY_SERVICE_ENPOINT,
        'stream_context' => stream_context_create(array('http' => array('header' => "x-kobo-Partner:" . X_KOBO_PARTNER . "\r\n" .
            "x-kobo-APIKey: " . X_KOBO_APIKEY . "")))
    );

    $client = new SoapClient(KOBO_LIBRARY_SERVICE, $options);

    try {

        $response = $client->RemoveFromLibrary(array(
            'request' => array(
                "ProductId" => $product_id,
                "UserId" => $user_id
            )
        ));

        //traduccion de result de kobo
        $translation = new Translations();

        //Guardando traduccion en resultado_translation
        $translationSpanish = $translation->spanish_api_translation($response->RemoveFromLibraryResult);

        //Creando array que tendra el mensaje de la traduccion
        $es_message = array('es' => $translationSpanish);

        //Agregando la traduccion al result de kobo
        $response->RemoveFromLibraryResult->Message = $es_message;

        $json = json_encode($response->RemoveFromLibraryResult);
        //set content type
        $app->response->setContentType('application/json', 'UTF-8');
        //echo response
        echo $json;
        
        $logger->log($client->__getLastRequestHeaders());
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());
        
    } catch (SoapFault $e) {
        $app->response->setStatusCode(401, "Bad Request")->send();
        echo json_encode(array("error" => $e->getMessage()));

        echo $e->getMessage();
        $logger->log($client->__getLastRequestHeaders());
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());
    }


});


/**
 * Funcion que regresa una lista de URLs de productos para un usuario determinado.
 */
$app->get('/library_service/get_download_urls/{product_id}/{user_id}', function ($product_id, $user_id) use ($app, $logger) {

    $options = array(
        'trace' => 1,
        'location' => KOBO_LIBRARY_SERVICE_ENPOINT,
        'stream_context' => stream_context_create(array('http' => array('header' => "x-kobo-Partner:" . X_KOBO_PARTNER . "\r\n" .
            "x-kobo-APIKey: " . X_KOBO_APIKEY . "")))
    );

    $client = new SoapClient(KOBO_LIBRARY_SERVICE, $options);

    try {

        $response = $client->GetDownloadUrls(array(
            'request' => array(
                "ProductId" => $product_id,
                "UserId" => $user_id
            )
        ));



        $json = json_encode($response->GetDownloadUrlsResult);
        //set content type
        $app->response->setContentType('application/json', 'UTF-8');
        //echo response
        echo $json;
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());

    } catch (SoapFault $e) {
        $app->response->setStatusCode(401, "Bad Request")->send();
        echo json_encode(array("error" => $e->getMessage()));

        echo $e->getMessage();
        $logger->log($client->__getLastRequestHeaders());
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());
    }

});
