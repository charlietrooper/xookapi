<?php
/**
 *
 * Sets handlers for the /weather/** routes
 *
 *
 * Created by PhpStorm.
 * User: AppStudio
 * Date: 14/05/2015
 * Time: 07:27 PM
 */

/**
 * Gets the weather for the given city
 */
$app->get('/weather/{country}/{city}', function ($country, $city) use ($app, $logger) {

    $requestParams = array(
        'CityName' => $city,
        'CountryName' => $country
    );

    $logger->log("Fetching weather for $country, $city");

    $client = new SoapClient('http://www.webservicex.net/globalweather.asmx?WSDL');
    $response = $client->GetWeather($requestParams);

    print_r($response);

    //Kill first line (webservice repsonse is malformed)
    $response->GetWeatherResult = preg_replace('/^.+\n/', '', $response->GetWeatherResult);
    //get json
    $jsonContents = xml2json::transformXmlStringToJson($response->GetWeatherResult);

    //set content type
    $app->response->setContentType('application/json', 'UTF-8');
    //echo response
    echo $jsonContents;

});

/**
 * Gets the list of cities for the given country
 */
$app->get('/cities/{country}', function ($country) use ($app, $logger) {
    $requestParams = array(
        'CountryName' => $country
    );

    $logger->log("Fetching cities for $country");

    $client = new SoapClient('http://www.webservicex.net/globalweather.asmx?WSDL');
    $response = $client->GetCitiesByCountry($requestParams);

    //get JSON from XML response
    $jsonContents = xml2json::transformXmlStringToJson($response->GetCitiesByCountryResult);

    //set content type
    $app->response->setContentType('application/json', 'UTF-8');
    //echo response
    echo $jsonContents;


});

$app->get('/uuid', function () use ($app, $logger) {

    $customer = new Customers();
    $uuid = $customer->genUUID();

    //set content type
    $app->response->setContentType('application/json', 'UTF-8');
    //echo response
    echo json_encode($uuid);


});
