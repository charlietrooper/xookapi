<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 17/11/2015
 * Time: 12:32 PM
 */

/**
 *
 *
 *
 * RUTA PARA CANCELAR UNA PRE-ORDER
 */
$app->get('/pre_order/cancel/{email}/{product_id}', function ($email, $product_id) use ($app, $logger) {

    $logger->log("parametros recividos de web $email $product_id");

    $partner = Partners::getPartnerByAPICredentials($app->request->getHeader(X_API_USER_HEADER), $app->request->getHeader(X_API_KEY_HEADER));
    $partner_id = $partner->partner_id;

    $logger->log("El partner id es $partner_id");

    $pre_order = PreOrder::findFirst(array(
        'email_customer = :email: AND product_id = :id: AND partner_id = :p_id: AND status = "Pendiente"',
        'bind' => array('email' => $email, 'id' => $product_id, 'p_id' => $partner_id)
    ));

    $pre_order->status = "Cancelado";

    $success = $pre_order->save();


    if ($success) {
        //Se llama a la función para actualizar el numero de libros vendidos y la suma de los costos
        $purchase_line = PurchasesLine::actualizaCantidadCosto($pre_order->product_id, $pre_order->purchase_id);
        $logger->log('Respuesta de la actualizacion de número de libros y costo: ' . $purchase_line);

        echo json_encode(array('PreOrderResult' => 'Orden Cancelada'));
    } else {
        foreach ($pre_order->getMessages() as $messages) {
            $logger->log($messages);

        }
        echo json_encode(array('Error' => 'Algo salio mal en la llamada'));
    }

});

/*
 *
 *
 *
 * RUTA PARA MOSTRAR EL LISTADO DE PRE-ORDER DE UN CLIENTE
 */
$app->get('/pre_order/list/{email}', function ($email) use ($app, $logger) {

    $partner = Partners::getPartnerByAPICredentials($app->request->getHeader(X_API_USER_HEADER), $app->request->getHeader(X_API_KEY_HEADER));
    $partner_id = $partner->partner_id;

    $logger->log('api_user: ' . $app->request->getHeader(X_API_USER_HEADER));
    $logger->log('api_key: ' . $app->request->getHeader(X_API_KEY_HEADER));


    $pre_order = PreOrder::find(array(
        'email_customer = :email: AND partner_id = :id:',
        'bind' => array('email' => $email, 'id' => $partner_id)
    ));

    $res = array();

    foreach ($pre_order as $key => $k) {
        $array = array('id' => $k->clv_pre_order,
            'product_id' => $k->product_id,
            'status' => $k->status,
            'fch_order' => $k->fch_order,
            'fch_pedido' => $k->fch_pedido,
            'fch_entrega' => $k->fch_entrega,
            'title' => $k->title_product,
            'isbn' => $k->isbn,
            'kobo_id' => $k->kobo_id);

        array_push($res, $array);

    }

    $logger->log(json_encode($res));

    echo json_encode($res);
});

/*
 * Ruta para activar la pre orden, la cual se ejecutará con éxito cuando
 * la fecha actual coincida con la fecha de activacion del libro
 */
$app->get('/pre_order/active', function () use ($app, $logger) {

    //Se obtiene la fecha actual
    $fecha = date('Y-m-d');

    //Buscamos la preOrder a partir de la fecha actual y estatus pendiente
    $pre_order = PreOrder::find(array(
        'CAST(fch_order as DATE) <= :fecha: AND status = "Pendiente"',
        'bind' => array('fecha' => $fecha)
    ));

    //se envía al log el numero de preordenes
    $logger->log('conteo de preordenes: ' . json_encode(count($pre_order)));

    if (count($pre_order) == null) {
        //si no existen ninguna preorden:
        //-la respuesta general indica que no hay libros para activar
        $respuesta = "No hay libros disponibles en la base de datos para activar en estos momentos";
        //-se envía al log la respuesta
        $logger->log(json_encode($respuesta));
    } else {
        //Si la preorden contiene algo, se inicia la activación de los libros encontrados

        $librosActivados = array();
        $librosNoActivados = array();

        //Se recorre la variable $pre_order
        foreach ($pre_order as $pre => $p) {
            unset($address);
            unset($product);

            $address = array();
            $product = array();

            //Se mandan al log el id del producto y el id de la compra
            $logger->log('Comienza activación para libro: ' . json_encode($p->product_id));
            $logger->log('Y con el id de venta : ' . json_encode($p->purchase_id));

            //Consultamos el purchase para mas informacion que se enviara a kobo
            $purchase = Purchases::findFirst(array(
                'purchase_id = :id:',
                'bind' => array('id' => $p->purchase_id)
            ));

            if ($purchase == null || count($purchase) == 0) {
                //Si la consulta de la venta arroja nulo o cero registros, se envía al log la falla y se ingresa
                //el libros al array de los libros no activados, continuando con la siguiente iteración
                $logger->error(json_encode('No se encontró el id de compra (' . $p->purchase_id . ') de libro con id: ' . $p->product_id));
                array_push($librosNoActivados, $p->product_id);
                continue;

            } else {
                //Se lleva a cabo la authentication
                $options = array(
                    'trace' => 1,
                    'location' => KOBO_AUTHENTICATION_SERVICE_ENPOINT,
                    'stream_context' => stream_context_create(array('http' => array('header' => "x-kobo-Partner: " . X_KOBO_PARTNER . "\r\n" .
                        "x-kobo-APIKey: " . X_KOBO_APIKEY . "")))
                );

                $client = new SoapClient(KOBO_AUTHENTICATION_SERVICE, $options);
                $aut = $client->AuthenticateOrRegister(array(
                    'request' => array(
                        "PartnerId" => $purchase->partner_id,
                        "Email" => $p->email_customer,
                        "PartnerUserId" => $purchase->orbile_customer_id
                    )
                ));

                //Si existe el id_kobo, se continúa con la activación
                if (isset($aut->AuthenticateOrRegisterResult->KoboUserId)) {
                    //Obtenemos el kobo_id
                    $user_id = $aut->AuthenticateOrRegisterResult->KoboUserId;

                    //Se envía al log los datos de la compra
                    $logger->log('Registro de venta(purchase):  ' . json_encode($purchase));

                    //Se define el nuevo id de compra
                    $purchaseId = "$purchase->purchase_partner_id-PRE-$p->clv_pre_order";

                    //Se define la dirección del cliente
                    $address = array('City' => $purchase->address_city,
                        'Country' => $purchase->address_country,
                        'StateProvince' => $purchase->address_state_provincy,
                        'ZipPostalCode' => $purchase->address_zip_postal_code);

                    //se obtienen los datos del detalle de compra para definir el producto a enviar
                    $Line = PurchasesLine::findFirst(array(
                        'purchase_id = :id_line: AND product_id = :p_id: AND result = "PreOrder"',
                        'bind' => array('id_line' => $p->purchase_id, 'p_id' => $p->product_id)
                    ));

                    $product[] = array(
                        'COGS' => array(
                            'Amount' => $Line->cogs_amount,
                            'CurrencyCode' => $Line->cogs_currency_code,
                        ),
                        'PriceCharged' => array(
                            'CurrencyCode' => $Line->price_currency_code,
                            'DiscountApplied' => $Line->price_discount_applied,
                            'PriceBeforeTax' => $Line->price_before_tax,
                            'Tax' => array(
                                'Amount' => '',
                                'Name' => '',
                            )
                        ),
                        'ProductId' => $p->product_id
                    );

                    //Se envían al log los datos del producto que se enviarán
                    $logger->log('datos del producto(purchases_line): ' . json_encode($product));

                    //Se llama a la api de kobo para realizar la compra
                    $options = array(
                        'trace' => 1,
                        'location' => KOBO_PURCHASING_SERVICE_ENPOINT,
                        'stream_context' => stream_context_create(array('http' => array('header' => "x-kobo-Partner: " . X_KOBO_PARTNER . "\r\n" .
                            "x-kobo-APIKey: " . X_KOBO_APIKEY . "")))
                    );

                    $client = new SoapClient(KOBO_PURCHASING_SERVICE, $options);

                    try {
                        //se indica en el log que se iniciara el llamado para la compra
                        $logger->log("llamando service de kobo");

                        //Se realiza la compra
                        $response = $client->Purchase(array(
                            'request' => array(
                                'Address' => $address,
                                'Products' => $product,
                                "PurchaseId" => $purchaseId,
                                "UserId" => $user_id
                            )
                        ));

                        //Se envía al log la respuesta de kobo
                        $logger->log("Respuesta de kobo para el libro " . json_encode($p->product_id) . " :" . json_encode($response));

                        $respuesta = $response;

                        //Si la respuesta de Kobo es 'Success' se culmina la compra
                        if (isset($respuesta->PurchaseResult->Items->Item->Result->Result) && $respuesta->PurchaseResult->Items->Item->Result->Result == 'Success') {
                            //Se asignan los valores a los detalles de compra y (en caso de no contar con ciertos valores
                            //en la respuesta de Kobo se definen manalmente:

                            if (isset($respuesta->PurchaseResult->Items->Item->Result->DownloadUrls->DownloadUrl->ContentFormat)) {
                                $Line->content_format = $respuesta->PurchaseResult->Items->Item->Result->DownloadUrls->DownloadUrl->ContentFormat;
                            } else {
                                $Line->content_format = "(not available)";
                            }

                            if (isset($respuesta->PurchaseResult->Items->Item->Result->DownloadUrls->DownloadUrl->Url)) {
                                $Line->download_url = $respuesta->PurchaseResult->Items->Item->Result->DownloadUrls->DownloadUrl->Url;
                            } else {
                                $Line->download_url = "(not available)";
                            }

                            $succes_line = $Line->save();

                            //Se envía al log si fue exitosa la actualizacion del registro
                            $logger->log("actualizacion de purchases_line: " . json_encode($succes_line));

                            //se odifican los datos de la preorden
                            $p->status = "Entregado";
                            $p->fch_entrega = date('Y-m-d H:m:s a');

                            $succes_preorder = $p->save();

                            //se envia a log si fue exitosa la actualización del registro
                            $logger->log("Terminó activación de pre_order: " . json_encode($succes_preorder));

                            //Se ingresa el libro al array de los libros activados
                            array_push($librosActivados, array(
                                'id_libro' => $p->product_id,
                                'datos_guardados_purchase_line' => $succes_line,
                                'datos_guardados_pre_order' => $succes_preorder
                            ));

                            //Se indica en el log que este libro se activo con éxito
                            $logger->log("Libro completado: " . json_encode($p->product_id));
                        } else {
                            //Se indica en el log que el libro no se completó debido a que kobo
                            //no realizo la compra y se agrega al array de los No activados
                            $logger->log('el libro '.json_encode($p->product_id). ' no se actuvo ya que kobo no devolvió "Success"');
                            array_push($librosNoActivados, $p->product_id);
                        }

                    } catch (SoapFault $e) {
                        $logger->log("Libro fallido: " . json_encode($p->product_id));

                        array_push($librosNoActivados, $p->product_id);

                        $logger->log($e->getMessage());
                        $logger->log($client->__getLastRequestHeaders());
                        $logger->log($client->__getLastRequest());
                        $logger->log($client->__getLastResponse());
                        die;
                    }
                } else {
                    //Se indica en el log que el libro no se activo ya que el kobo_id estuvo vacío
                    $logger->error("Libro no activado debido a que el Kobo Id  está vacío para ".json_encode($p->email_customer));
                    array_push($librosNoActivados, $p->product_id);
                    continue;
                }
            }
        }
        //se juntan los array de los libros SI y No activados para enviarlos a la respuesta
        $respuesta = array(
            'libros_activados' => $librosActivados,
            'libros_no_activados' => $librosNoActivados
        );
    }

    //se imprime la respuesta
    echo json_encode($respuesta);
});
