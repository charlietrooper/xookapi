<?php
/**
 * Created by PhpStorm.
 * User: rrendon
 * Date: 20/05/2015
 * Time: 12:12 PM
 */

include("lib/WSDL/CheckLoginResponse.php");
include("lib/WSDL/CheckLoginRequest.php");
include("lib/WSDL/CheckLoginServer.php");

/**
 *
 * funcion que llama al webservice
 *
 */
$app->post('/authservice', function () use ($logger) {
    $classmap = array('CheckLoginResponse' => 'CheckLoginResponse', 'CheckLoginRequest' => 'CheckLoginRequest');
    $server = new SoapServer('Authentication.wsdl', array('classmap' => $classmap));
    $server->setClass("CheckLoginServer", $logger);
    $server->handle();

});
/**
 * funcion que llama al archivo Authentication.wsdl
 */
$app->get('/authservice/wsdl', function () use ($app) {
    header('Location: ../Authentication.wsdl');
});