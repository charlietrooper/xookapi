<?php

/**
 * Funcion para verificar existencia del usuario, existencia del contenido y si el contenido puede ser vendido al usuario
 */
$app->post('/purchase_service/verify_purchase', function () use ($app, $logger) {

    $rawData = file_get_contents("php://input");

    if ($rawData == null) {
        $app->response->setStatusCode(401, "Bad Request")->send();
        echo json_encode(array("error" => "POST body null"));
        die();
    } else {
        $input = json_decode($rawData);

        if ($input == null) {
            $logger->error("JSON DECODE FAILED");
            $app->response->setStatusCode(401, "Bad Request")->send();
            echo json_encode(array("error" => "JSON DECODE FAILED"));
            die();
        }

        if (empty($input->user_id)) {
            $logger->error("EMPTY USER ID");
            $app->response->setStatusCode(401, "Bad Request")->send();
            echo json_encode(array("error" => "EMPTY USER ID"));
            die();
        }

        $user_id = $input->user_id;
        $product_ids = $input->product_ids;
        $address = $input->address;

        $logger->debug("Input: " . $rawData);

    }

    $options = array(
        'trace' => 1,
        'location' => KOBO_PURCHASING_SERVICE_ENPOINT,
        'stream_context' => stream_context_create(array('http' => array('header' => "x-kobo-Partner: " . X_KOBO_PARTNER . "\r\n" . "x-kobo-APIKey: " . X_KOBO_APIKEY . "")))
    );

    $client = new SoapClient(KOBO_PURCHASING_SERVICE, $options);

    try {
        $response_kobo = $client->VerifyPurchase(array(
            'request' => array(
                'Address' => $address,
                'ProductIds' => $product_ids,
                "UserId" => $user_id
            )
        ));

        //En caso de que el no se array lo convertiremos a array
        if (!is_array($response_kobo->VerifyPurchaseResult->Items->Item)) {
            $response_kobo->VerifyPurchaseResult->Items->Item = array($response_kobo->VerifyPurchaseResult->Items->Item);
        }

        $Items = $response_kobo->VerifyPurchaseResult->Items->Item;

        //AQUI INICIARA LA BUSQUEDA DE LOS LIBROS EN EL FEED PARA BUSCAR PRE-ORDER

        //Llamamos la clase para la ruta de catalog
        $callFeed = New CallFeed();

        $success = 0;

        //Recorremos el Item de respuesta kobo
        foreach ($Items as $key => $I) {
            $logger->log("Producto id " . $I->Id);

            //Mandamos el id del libro para saber si isPreOrder
            $isPreOrder = $callFeed->VerifyActiveRev($I->Id);

            //lo mandamos al log esto no interfiere en nada
            $logger->log("¿Es pre-orden? $isPreOrder");

            //Anidamos la el resultado de preorder al json de kobo
            $I->isPreOrder = $isPreOrder;

            //Se busca que no haya una compra de esta preorden con estatus en pendiente
            if ($isPreOrder == 'true') {
                $customer = Customers::findFirst(array(
                    "kobo_id = :user_id:",
                    "bind" => array('user_id' => $user_id)
                ));

                if ($customer) {
                    $venta = PreOrder::findFirst(array(
                        "product_id = :id: AND email_customer = :email: AND status = 'Pendiente'",
                        "bind" => array('id' => $I->Id, 'email' => $customer->email)
                    ));

                    if ($venta == false) {
                        unset($I->Result);
                        $I->Result = "Success";
                    } else {
                        unset($I->Result);
                        $I->Result = "ContentAlreadyOwned";
                        $success = $success + 1;
                    }
                } else {
                    $logger->error("Customer Not Found");
                    $app->response->setStatusCode(401, "Bad Request")->send();
                    echo json_encode(array("error" => "Customer Not Found"));
                    die();
                }
            }
        }

        //Se cambia el resultado general en caso de que se haya duplicado un preorder
        if ($success > 0) {
            unset($response_kobo->VerifyPurchaseResult->Result);
            $response_kobo->VerifyPurchaseResult->Result = 'InternalError';
        }

        //traduccion de result de kobo
        $translation = new Translations();

        //Guardando traduccion en resultado_translation
        $translationSpanish = $translation->spanish_api_translation($response_kobo->VerifyPurchaseResult->Result);

        //Creando array que tendra el mensaje de la traduccion
        $es_message = array('es' => $translationSpanish);

        //Anidamos la traduccion al result de kobo
        $response_kobo->VerifyPurchaseResult->Message = $es_message;

        //Anadimos los resultados del foreach con el de kobo y lo enviamos
        $response_kobo->VerifyPurchaseResult->Items->Item = $Items;

        $json = json_encode($response_kobo->VerifyPurchaseResult);

        //set content type
        $app->response->setContentType('application/json', 'UTF-8');

        echo $json;

        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());

    } catch (SoapFault $e) {
        $app->response->setStatusCode(401, "Bad Request")->send();
        echo json_encode(array("error" => $e->getMessage()));

        $logger->log($e->getMessage());
        $logger->log($client->__getLastRequestHeaders());
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());
        die;
    }
});


/**
 *Funcion para ejecutar compra, se actualiza la biblioteca del usuario y se registra la compra
 */

$app->post('/purchase_service/purchase', function () use ($app, $logger) {
    /*************************************************
     * 1. Obtener los datos del request
     **************************************************/

    //se obtiene los datos enviados por el partner
    $rawData = file_get_contents("php://input");

    if ($rawData == null) {
        //Si esta vacía la variable de los datos recibidos del partner, se manda error
        $app->response->setStatusCode(401, "Bad Request")->send();
        echo json_encode(array("error" => "POST body null"));
        die();
    } else {
        //Se acomodan los datos obtenidos
        $input = json_decode($rawData);

        $user_id = "";
        if (isset($input->user_id)) {
            $user_id = $input->user_id;
        }


        if (isset($input->UserId)) {
            $user_id = $input->UserId;
        }

        if ($user_id == "") {
            $app->response->setStatusCode(401, "Bad Request")->send();
            echo json_encode(array("error" => "user_id missing"));
            die();
        }

        $purchase_id = $input->PurchaseId;
        $products = $input->Products;
        $address = $input->Address;

        if (isset($input->payment)) {
            $payment = implode('', $input->payment);

            if (empty(trim($payment))) {
                $payment = "NO definido";
            }
        } else {
            $payment = "NO definido";
        }

        $address_city = $address->City;
        $address_country = $address->Country;
        $address_state = $address->StateProvince;
        $addres_zip_postal_code = $address->ZipPostalCode;

        $logger->log('Datos obtenidos del partner: ' . json_encode($rawData));
    }

    /*************************************************
     * 2. Separar productos en preorders y no preorders
     **************************************************/

    //Se llama a  la clase callFeed
    $callFeed = new CallFeed();

    $libros = $callFeed->separarLibros($products);

    //Se crea un array para meter a los libros que son pre-órdenes
    $booksPreorder = $libros[0];

    //Se crea un array para los libros que no son pre-órdenes
    $booksPartner = $libros[1];

    $logger->log('Productos a solicitar en la compra: ' . json_encode($products));

    //Se imprimime el resultado de ambos arrays
    $logger->log("Arreglo de libros que son pre-órdenes: " . json_encode($booksPreorder));
    $logger->log("Arreglo de libros que no son pre-órdenes: " . json_encode($booksPartner));

    //Se obtienen los datos del partner
    $partner = Partners::getPartnerByAPICredentials($app->request->getHeader(X_API_USER_HEADER), $app->request->getHeader(X_API_KEY_HEADER));
    $partner_id = $partner->partner_id;

    try {
        /*************************************************
         * 3. Procesar los productos que NO son pre-ordenes
         **************************************************/
        
        //Init soap client
        $options = array(
                'trace' => 1,
                'location' => KOBO_PURCHASING_SERVICE_ENPOINT,
                'stream_context' => stream_context_create(array('http' => array('header' => "x-kobo-Partner: " . X_KOBO_PARTNER . "\r\n" .
                    "x-kobo-APIKey: " . X_KOBO_APIKEY . "")))
            );
        $client = new SoapClient(KOBO_PURCHASING_SERVICE, $options);
                
        //Si se registraron libros que NO son pre-órdenes, se realiza la llamada a Kobo
        if (!empty($booksPartner)) {
            $response = $client->Purchase(array(
                'request' => array(
                    'Address' => $address,
                    'Products' => $booksPartner,
                    "PurchaseId" => "$partner->order_prefix-$purchase_id",
                    "UserId" => $user_id
                )
            ));
            $respuesta = $response;
            $logger->log('Respuesta de la llama a Kobo: ' . json_encode($response));
            $respuesta->PurchaseResult->Items = $respuesta->PurchaseResult->Items->Item;
            if (!is_array($respuesta->PurchaseResult->Items)) {
                $respuesta->PurchaseResult->Items = array($respuesta->PurchaseResult->Items);
            }
            $items = $respuesta->PurchaseResult->Items;
        } //Si NO se registraron libros que No sean pre-órdenes, se genera estructura de la respuesta manualmente
        else {
            
            //Creamos un objeto en el cual se anidaran las pre-órdenes
            $respuesta = (object)array('PurchaseResult' => (object)array(
                'Items' => (object)array(
                    'Item' => array()
                ),
                'Result' => 'Success'
            ));
            $respuesta->PurchaseResult->Items = $respuesta->PurchaseResult->Items->Item;
            $logger->log("Respuesta generada para pre-órdenes: " . json_encode($respuesta));
        }

        //Se obtienen los datos del cliente
        $customer = Customers::findFirst(array(
            "kobo_id = :user_id:",
            "bind" => array('user_id' => $user_id)
        ));


        /*************************************************
         * 4. Procesar los que son pre-ordenes
         **************************************************/

        //Si existen pre-órdenes se generan los arrays de respuesta
        if (!empty($booksPreorder)) {
            //Se declara la variable que nos dirá si algun lirbos en pre-órden no resulta exitoso
            $success = 0;

            //Se recorren las pre-órdenes
            foreach ($booksPreorder as $valor) {
                unset($venta);
                unset($pre);
                $pre = (object)[];
                $logger->log('Se formará la respuesta de la pre-órden con id: ' . json_encode($valor->ProductId));

                //SE verifica si dicha pre-orden ya la contiene el cliente
                $venta = PreOrder::findFirst(array(
                    "product_id = :id: AND email_customer = :email: AND status = 'Pendiente'",
                    "bind" => array('id' => $valor->ProductId, 'email' => $customer->email)
                ));

                //Se agregan los datos del libro y su resultado a la respuesta
                if ($venta == false) {
                    $pre->Id = $valor->ProductId;
                    $pre->ContentFormat = "EPUB";
                    $pre->PreOrderResult = "Success";
                    array_push($respuesta->PurchaseResult->Items, $pre);
                } else {
                    $pre->Id = $valor->ProductId;
                    $pre->ContentFormat = "EPUB";
                    $pre->PreOrderResult = "PreOrderAlreadyInPlace";
                    array_push($respuesta->PurchaseResult->Items, $pre);
                    $success = $success + 1;
                }
                $logger->log('Respuesta formada para el libro indicado: ' . json_encode($pre));
            }
            $logger->log('Respuesta completada anexando ls pre-órdenes: ' . json_encode($respuesta->PurchaseResult->Items));

            //Si alguna preorden está repetida, manda internal error la respuesta general
            if ($success > 0) {
                unset($respuesta->PurchaseResult->Result);
                $respuesta->PurchaseResult->Result = 'InternalError';
                $logger->log("Respuesta completada y verificada en el resultado " . json_encode($respuesta));
            }
        }

        //Se revisa en la bd si el id de compra ya existe
        $purchase_c = Purchases::findFirst(array("purchase_partner_id=:purchase_partner_id:", 'bind' => array("purchase_partner_id" => $purchase_id)));

        //Si el id de compra ya existe se manda mensaje de error y se corta el proceso
        if ($purchase_c) {
            $app->response->setStatusCode(401, "Bad Request")->send();
            echo json_encode(array("error" => "ID purchase duplicado"));
            die;
        }

        /*************************************************
         * 5. Se crea el Purchase
         **************************************************/

        //se registra la compra en la bd
        $purchase = new Purchases();
        $purchase->partner_id = $partner_id;
        $purchase->user_id = $user_id;
        $purchase->purchase_partner_id = "$partner->order_prefix-$purchase_id";
        $purchase->date = date("Y-m-d H:i:s");
        $purchase->address_city = $address_city;
        $purchase->address_country = $address_country;
        $purchase->address_state_provincy = $address_state;
        $purchase->address_zip_postal_code = $addres_zip_postal_code;
        $purchase->result = $respuesta->PurchaseResult->Result;
        $purchase->payment_method = $payment;
        $purchase->orbile_customer_id = $customer->orbile_customer_id;

        //Se agrega la cantidad de libros comprados y el total de costo vendido
        $sum = 0;
        $cont = 0;
        if (!empty($booksPartner)) {
            for ($i = 0; $i < count($booksPartner); $i++) {
                if ($items[$i]->Result->Result == 'Success') {
                    $sum = $sum + (int)$booksPartner[$i]->PriceCharged->PriceBeforeTax;
                    $cont = $cont + 1;
                }
            }
        }
        if (!empty($booksPreorder)) {
            foreach ($booksPreorder as $n => $vaa) {
                foreach ($respuesta->PurchaseResult->Items as $libp) {
                    if ($vaa->ProductId == $libp->Id) {
                        if ($libp->PreOrderResult == 'Success') {
                            $sum = $sum + (int)$vaa->PriceCharged->PriceBeforeTax;
                            $cont = $cont + 1;
                        }
                    }
                }
            }
        }
        $purchase->purchase_book_num = $cont;
        $logger->log('purchase_book_num: ' . $cont);
        $purchase->purchase_sum = $sum;
        $logger->log('purchase_sum: ' . $sum);

        $purchase->save();
        //Compra guardada


        /*************************************************
         * 6. Se crean los purchases line para los NO pre-ordenes
         **************************************************/

        if (!empty($booksPartner)) {
            //Se registra el detalle de los productos de la compra
            for ($i = 0; $i < count($booksPartner); $i++) {

                $datos_libro = $callFeed->ObtenerDatosLibro($items[$i]->Id);

                $purchase_line = new PurchasesLine();
                $purchase_line->purchase_id = $purchase->purchase_id;
                $purchase_line->cogs_amount = $booksPartner[$i]->COGS->Amount;
                $purchase_line->cogs_currency_code = $booksPartner[$i]->COGS->CurrencyCode;
                $purchase_line->price_currency_code = $booksPartner[$i]->PriceCharged->CurrencyCode;
                $purchase_line->price_discount_applied = $booksPartner[$i]->PriceCharged->DiscountApplied;
                $purchase_line->price_before_tax = $booksPartner[$i]->PriceCharged->PriceBeforeTax;
                $purchase_line->item_id = $items[$i]->Id;
                $purchase_line->product_id = $items[$i]->Id;

                //Sometimes the response from Kobo does not include this info
                if (isset($items[$i]->Result->DownloadUrls->DownloadUrl->ContentFormat)) {
                    $purchase_line->content_format = $items[$i]->Result->DownloadUrls->DownloadUrl->ContentFormat;
                } else {
                    $purchase_line->content_format = "(not available)";
                }

                if (isset($items[$i]->Result->DownloadUrls->DownloadUrl->Url)) {
                    $purchase_line->download_url = $items[$i]->Result->DownloadUrls->DownloadUrl->Url;
                } else {
                    $purchase_line->download_url = "(not available)";
                }


                $purchase_line->result = $items[$i]->Result->Result;
                if ($datos_libro != 'error') {
                    $purchase_line->idioma = $datos_libro['idioma'];
                    $purchase_line->genero = $datos_libro['genero'];
                    $purchase_line->is_kids = $datos_libro['isKids'];
                    $purchase_line->is_adult_material = $datos_libro['isAdultMaterial'];
                    $purchase_line->cogs = $datos_libro['cogs'];
                    $purchase_line->titulo = $datos_libro['titulo'];
                    $purchase_line->isbn = $datos_libro['isbn'];
                    $purchase_line->genero_bic = $datos_libro['Generos']['BIC'];
                    $purchase_line->genero_bisac = $datos_libro['Generos']['BISAC'];
                    $purchase_line->account_holder_id = $datos_libro['accountHolderId'];
                    $purchase_line->imprint = $datos_libro['imprint'];
                }
                $purchase_line->save();
            }
            //Detalles guardados
        }


        /*************************************************
         * 7. Se crean los registros en puchases line y en pre order para las pre-ordenes
         **************************************************/

        //Si existen preordenes se realizan los respectivos registros para su futura activación/compra
        if (!empty($booksPreorder)) {
            foreach ($booksPreorder as $name => $v) {
                $datosLibro = $callFeed->ObtenerDatosLibro($v->ProductId);

                $purchaseLine = new PurchasesLine();
                $purchaseLine->purchase_id = $purchase->purchase_id;
                $purchaseLine->cogs_amount = $v->COGS->Amount;
                $purchaseLine->cogs_currency_code = $v->COGS->CurrencyCode;
                $purchaseLine->price_currency_code = $v->PriceCharged->CurrencyCode;
                $purchaseLine->price_discount_applied = $v->PriceCharged->DiscountApplied;
                $purchaseLine->price_before_tax = $v->PriceCharged->PriceBeforeTax;
                $purchaseLine->item_id = $v->ProductId;
                $purchaseLine->product_id = $v->ProductId;

                foreach ($respuesta->PurchaseResult->Items as $lib) {
                    if ($v->ProductId == $lib->Id) {
                        if ($lib->PreOrderResult == 'Success') {
                            $purchaseLine->result = "PreOrder";
                        } else {
                            $purchaseLine->result = "PreOrderAlreadyInPlace";
                        }
                    }
                }
                $purchaseLine->content_format = "(not available)";
                $purchaseLine->download_url = "(not available)";
                if ($datosLibro != 'error') {
                    $purchaseLine->idioma = $datosLibro['idioma'];
                    $purchaseLine->genero = $datosLibro['genero'];
                    $purchaseLine->is_kids = $datosLibro['isKids'];
                    $purchaseLine->is_adult_material = $datosLibro['isAdultMaterial'];
                    $purchaseLine->cogs = $datosLibro['cogs'];
                    $purchaseLine->titulo = $datosLibro['titulo'];
                    $purchaseLine->isbn = $datosLibro['isbn'];
                    $purchaseLine->genero_bic = $datosLibro['Generos']['BIC'];
                    $purchaseLine->genero_bisac = $datosLibro['Generos']['BISAC'];
                    $purchaseLine->account_holder_id = $datosLibro['accountHolderId'];
                    $purchaseLine->imprint = $datosLibro['imprint'];
                }
                $purchaseLine->save();
            }
            //Detalles guardados

            $libros = $respuesta->PurchaseResult->Items;

            //Se almacenan los datos de preorder en la tabla PreOrder
            for ($i = 0; $i < count($libros); $i++) {
                if ($libros[$i]->PreOrderResult == 'Success') {
                    //Se llama a la funcion 'PreOrder' de la clase 'CallFeed' para traer mas detalles de PreOrder
                    $datosFeed = $callFeed->PreOrder($libros[$i]->Id);

                    $pre_order = new PreOrder();
                    $pre_order->partner_id = $partner_id;
                    $pre_order->purchase_id = $purchase->purchase_id;
                    $pre_order->product_id = $libros[$i]->Id;
                    $pre_order->email_customer = $customer->email;
                    $pre_order->status = "Pendiente";
                    $pre_order->fch_order = $datosFeed['date'];
                    $pre_order->fch_pedido = date('Y-m-d H:m:s a');
                    $pre_order->title_product = $datosFeed['title'];
                    $pre_order->isbn = $datosFeed['isbn'];
                    $pre_order->kobo_id = $user_id;

                    $pre_order->save();
                }
            }
            //Pre-órdenes guardadas
        }


        /*************************************************
         * 8. Se genera la respuesta json y se envia al partner
         **************************************************/

        //traduccion de result de kobo
        $translation = new Translations();

        //Guardando traduccion en resultado_translation
        $translationSpanish = $translation->spanish_api_translation($respuesta->PurchaseResult->Result);

        //Creando array que tendra el mensaje de la traduccion
        $es_message = array('es' => $translationSpanish);

        //Anidando la traduccion al result de kobo
        $respuesta->PurchaseResult->Message = $es_message;

        $api_user = $app->request->getHeader(X_API_USER_HEADER);

        $logger->log('Respuesta final con traducción: ' . json_encode($respuesta));

        //Se asigna la respuesta a la variable que posteriormente se devolverá al partner con la respuesta generada
        if ($api_user == "porrua") {
            $json = json_encode($respuesta);
        } else {
            $json = json_encode($respuesta->PurchaseResult);
        }

        $app->response->setContentType('application/json', 'UTF-8');

        //Se retorna el resultado solicitado
        echo $json;
    } catch (SoapFault $e) {
        $app->response->setStatusCode(401, "Bad Request");
        echo json_encode(array("error" => $e->getMessage()));

        $logger->log($e->getMessage());
        $logger->log($client->__getLastRequestHeaders());
        $logger->log($client->__getLastRequest());
        $logger->log($client->__getLastResponse());
    }
});
