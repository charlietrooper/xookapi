<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 28/04/2016
 * Time: 07:06 PM
 */
/**
 * Funcion para verificar existencia del usuario, existencia del contenido y si el contenido puede ser vendido al usuario
 */
$app->get('/sales/repair_extra_data', function () use ($app, $logger) {

    $purchases_line = PurchasesLine::find(array(
        "idioma IS NULL",
        "columns"   => "product_id",
        "group"     => "product_id",

    ));

    $callFeed = new CallFeed();

    /*$info['libros'] = array();*/
    $contador = 0;
    $num_control = 1000;
    foreach($purchases_line as $pl){
        //$numero = $numero+ 1;
        //$array['numero'][] = $numero;
        $info_datos = $callFeed->ObtenerDatosLibro($pl->product_id);
        if($info_datos == 'error'){
            $logger->error('hubo un error en la conexión, o no se encontró el libro con el id: ' . $pl->product_id);
        }
        else{
            $idioma = $info_datos['idioma'];
            $cogs = $info_datos['cogs'];
            $isAdultMaterial = $info_datos['isAdultMaterial'];
            $isKids = $info_datos['isKids'];
            $titulo = $info_datos['titulo'];
            $isbn = $info_datos['isbn'];
            $g_bic = $info_datos['Generos']['BIC'];
            $g_bisac = $info_datos['Generos']['BISAC'];
            $genero = $info_datos['genero'];
            $accountHolderId = $info_datos['accountHolderId'];
            $imprint = $info_datos['imprint'];


            $insercion = $app->modelsManager->executeQuery("UPDATE PurchasesLine SET idioma = :idioma:,
                                                                                      is_kids = :isKids:,
                                                                                      is_adult_material = :isAdultMaterial:,
                                                                                      cogs = :cogs:,
                                                                                      titulo = :titulo:,
                                                                                      isbn = :isbn:,
                                                                                      genero = :genero:,
                                                                                      genero_bic = :g_bic:,
                                                                                      genero_bisac = :g_bisac:,
                                                                                      account_holder_id = :accountHolderId:,
                                                                                      imprint = :imprint:
                                                             WHERE product_id = :product_id:",
                                                            array(
                                                                'idioma'            => $idioma,
                                                                'isKids'            => $isKids,
                                                                'isAdultMaterial'   => $isAdultMaterial,
                                                                'cogs'              => $cogs,
                                                                'titulo'            => $titulo,
                                                                'genero'            => $genero,
                                                                'isbn'              => $isbn,
                                                                'g_bic'             => $g_bic,
                                                                'g_bisac'           => $g_bisac,
                                                                'accountHolderId'   => $accountHolderId,
                                                                'imprint'           => $imprint,
                                                                'product_id'        => $pl->product_id));

            //$inf = array(
            //    'id'    => $info_id,
            //    'datos' => $info_datos
            //);
            //array_push($info['libros'], $inf);

            /*if ($insercion->success() == true) {
                $inf = array(
                    'id'    => $pl->product_id,
                    'info'  => $info_datos,
                    'inserción' => 'ok'
                );
                $logger->debug('query: '.json_encode('ok'));
            }
            else*/if ($insercion->success() == false) {
                /*$inf = array(
                    'id'    => $pl->product_id,
                    'info'  => $info_datos,
                    'inserción' => 'error'
                );*/
                foreach ($insercion->getMessages() as $message) {
                    $logger->debug('query - mensajes de error: ' . $message->getMessage());
                }
            }

            $contador = $contador + 1;
            if(($contador%$num_control) == 0)
            {
                $memory_usage = memory_get_usage(true)/(1024*1024);
                $memory_peak = memory_get_peak_usage(true)/(1024*1024);
                $logger->log('Líneas procesadas' . $contador . 'Memory_usage: ' . $memory_usage . 'memory_peak: ' . $memory_peak);
            }
            /*array_push($info['libros'], $inf);*/
        }
    }

    //$info = $callFeed->fetchBookMetadata('4462f303-a044-42e8-9228-57fc4626c585');
    //$logger->debug(json_encode($info));
    /*$logger->debug(json_encode($info));
    echo json_encode($info);*/
});

/*
 * Función de prueba para obtener los datos de un libro además de obtener la descripción de su genero
 */
$app->get('/sales/update_genero', function () use ($app, $logger) {
    $null = null;
    $purchases_line = PurchasesLine::find(array(
        "columns"   => "SUBSTRING(genero_bic,0,2), SUBSTRING(genero_bisac,0,3)",
        "group"     => "genero_bic, genero_bisac"
    ));

    $array = array();
    foreach($purchases_line as $pl){

        $array['genero bic:'][] = $pl->genero_bic;
        $array['genero bisac:'][] = $pl->genero_bisac;
    }
    $logger->log(json_encode($array));
    echo json_encode($array);
});

/*
 * Función de prueba para obtener los datos de un libro además de obtener la descripción de su genero
 */
$app->get('/sales/data_purchase', function () use ($app, $logger) {
    $id = '05070aa6-66f2-455f-874a-ef2b9e76c0df';//'09839f19-f94f-46be-a1a0-fba1a5154ebb';
    $callFeed = new CallFeed();
    $datos = $callFeed->ObtenerDatosLibro($id);
    echo json_encode($datos);
});


$app->get('/sales/updateMetadata', function () use ($app, $logger) {

    $purchases_line = PurchasesLine::find(array(
        "columns"   => "product_id",
        "group"     => "product_id",

    ));

    $callFeed = new CallFeed();

    /*$info['libros'] = array();*/
    $contador = 0;
    $num_control = 1000;
    foreach($purchases_line as $pl){
        //$numero = $numero+ 1;
        //$array['numero'][] = $numero;
        $info_datos = $callFeed->ObtenerDatosLibro($pl->product_id);
        if($info_datos == 'error'){
            $logger->error('hubo un error en la conexión, o no se encontró el libro con el id: ' . $pl->product_id);
        }
        else{
            $accountHolderId = $info_datos['accountHolderId'];
            $imprint = $info_datos['imprint'];


            $insercion = $app->modelsManager->executeQuery("UPDATE PurchasesLine SET  account_holder_id = :accountHolderId:,
                                                                                      imprint = :imprint:
                                                                                      WHERE product_id = :product_id:",
                array(
                    'accountHolderId'   => $accountHolderId,
                    'imprint'           => $imprint,
                    'product_id'        => $pl->product_id));

            //$inf = array(
            //    'id'    => $info_id,
            //    'datos' => $info_datos
            //);
            //array_push($info['libros'], $inf);

            /*if ($insercion->success() == true) {
                $inf = array(
                    'id'    => $pl->product_id,
                    'info'  => $info_datos,
                    'inserción' => 'ok'
                );
                $logger->debug('query: '.json_encode('ok'));
            }
            else*/if ($insercion->success() == false) {
                /*$inf = array(
                    'id'    => $pl->product_id,
                    'info'  => $info_datos,
                    'inserción' => 'error'
                );*/
                foreach ($insercion->getMessages() as $message) {
                    $logger->debug('query - mensajes de error: ' . $message->getMessage());
                }
            }

            $contador = $contador + 1;
            if(($contador%$num_control) == 0)
            {
                $memory_usage = memory_get_usage(true)/(1024*1024);
                $memory_peak = memory_get_peak_usage(true)/(1024*1024);
                $logger->log('Líneas procesadas' . $contador . 'Memory_usage: ' . $memory_usage . 'memory_peak: ' . $memory_peak);
            }
            /*array_push($info['libros'], $inf);*/
        }
    }

    //$info = $callFeed->fetchBookMetadata('4462f303-a044-42e8-9228-57fc4626c585');
    //$logger->debug(json_encode($info));
    /*$logger->debug(json_encode($info));
    echo json_encode($info);*/
});