<?php

/**
 * Created by PhpStorm.
 * User: AppStudio
 * Date: 19/05/2015
 * Time: 07:21 PM
 */


class CheckLoginServer
{
    var $logger;

    function CheckLoginServer($logger)
    {
     $this->logger = $logger;
    }
    /**
     * @param CheckLoginRequest $login_request
     * @return CheckLoginResponse
     *Funcion que verifica si el usuario y el password son correctos en el soap de gandhi y porrua
     */

    function CheckLogin($login_request)
    {


        $this->logger->log($login_request->Email);

        $email = $login_request->Email;
        $password = $login_request->Password;
        if(empty($password))
        {
            $this->logger->log('blanco');
        }
        else
        {
            $this->logger->log('*******');
        }
        try {

            //se crea curl para conectarse al webservice de porrua
            $service_url = 'https://porrua.mx/webservice/AuthenticationServiceRest.php?metodo=CheckLogin&Email=' . $email . '&Password=' . rawurlencode($password);


            $curl = curl_init($service_url);
            $curl_post_data = array(
                "Email" => $email,
                "Password" => rawurlencode($password),
            );
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
            $curl_response = curl_exec($curl);


            if ($curl_response === FALSE) {
                $error = curl_error($curl);
                $this->logger->log("cURL Error: $error");


            } else {
                $response_porrua = json_decode($curl_response, true);
                $this->logger->log("Respuesta Porrúa: $curl_response");
                //


                $status_porrua = $response_porrua['Status'];
                $ref_porrua = $response_porrua['UserRef'];

                curl_close($curl);


                ///cliente para conectarse al webservice de gandhi
                try
                {
                    $client = new SoapClient(GANDHI_WSDL_PATH);

                    $session = $client->login(GANDHI_USER_AUTH, GANDHI_PASS_AUTH);

                    if ($password == null) {
                        $gandhi_response = $client->call($session, 'customer.authenticate', array($email));
                    } else {
                        $gandhi_response = $client->call($session, 'customer.authenticate', array($email, $password));
                    }
                    
                    $client->endSession($session);

                    $status_gandhi = $gandhi_response['success'];
                    $ref_gandhi = $gandhi_response['reference'];
                    $this->logger->log("Respuesta Gandhi: $status_gandhi $ref_gandhi");
                }
                catch(Exception $e)
                {
                    $this->logger->log($e->getMessage());
                    $this->logger->log($e->getTraceAsString());
                    $this->logger->log('Error WS GANDHI');
                   
                    $status_gandhi = 0;
                    $ref_gandhi = null;
                }
                //se busca el email del cliente en la bd de orbile, si no existe se registra

                $customer = Customers::findfirst(array("email=:email:", 'bind' => array("email" => $email)));


                if (!$customer) {
                    //El usuario solamente esta en Porrúa
                    if ($status_porrua == 0 && $status_gandhi == 0) {
                        $partner_id = 27;
                        $customer = new Customers();
                        $customer->partner_id = $partner_id;
                        $customer->email = $email;
                        $customer->register_date = date("Y-m-d H:i:s");
                        $customer->partner_customer_id = $ref_porrua;
                        $customer->orbile_customer_id = $customer->genUUID();
                        $customer->account_type = 'P';
                        $customer->save();
                    } elseif ($status_porrua == 1 && $status_gandhi == 1) {
                        //El usuario solamente esta en Gandhi
                        $partner_id = 26;
                        $customer = new Customers();
                        $customer->partner_id = $partner_id;
                        $customer->email = $email;
                        $customer->register_date = date("Y-m-d H:i:s");
                        $customer->partner_customer_id = $ref_gandhi;
                        $customer->orbile_customer_id = $customer->genUUID();
                        $customer->account_type = 'P';
                        $customer->save();
                    } elseif ($status_porrua == 0 && $status_gandhi == 1) {
                        //El usuario esta ambos, comparte orbile_customer_id
                        $customer_g = new Customers();
                        $u_id = $customer_g->genUUID();
                        $customer_g->partner_id = 26;
                        $customer_g->email = $email;
                        $customer_g->register_date = date("Y-m-d H:i:s");
                        $customer_g->partner_customer_id = $ref_gandhi;
                        $customer_g->orbile_customer_id = $u_id;
                        $customer_g->account_type = 'P';
                        $customer_g->save();

                        $customer_p = new Customers();
                        $customer_p->partner_id = 27;
                        $customer_p->email = $email;
                        $customer_p->register_date = date("Y-m-d H:i:s");
                        $customer_p->partner_customer_id = $ref_gandhi;
                        $customer_p->orbile_customer_id = $u_id;
                        $customer_p->account_type = 'P';
                        $customer_p->save();

                    }
                    elseif($status_porrua == 1 && $status_gandhi == 0){
                        //No es usuario de nigún partner fin.
                        return new CheckLoginResponse(1, null,null);
                    }

                }
                //En este punto, cualquier sea el caso el usuraio ya debe de existir, traemos sus datos
                $customer = Customers::findfirst(array("email=:email:", 'bind' => array("email" => $email)));
                $orbile_id = $customer->orbile_customer_id;

                $customer_p = Customers::findFirst(array(
                        "orbile_customer_id=:orbile_customer_id: AND account_type = :account_type:",
                        "bind" => array("orbile_customer_id" => $orbile_id, "account_type" => 'P'))
                );

                $this->logger->log("Estatus Porrúa: ".($status_porrua == 0 ? 'SI':'NO'));
                $this->logger->log("Estatus Gandhi: ".($status_gandhi == 1 ? 'SI':'NO'));

                //la cuenta es secundaria

                if (!$customer_p) {
                    //la cuenta pertenece a porrua, regresa status 0, orbile_id de la cuenta primaria y correo de la cuenta primaria
                    if ($status_porrua == 0 && $status_gandhi == 0) {

                        return new CheckLoginResponse(0, $customer->orbile_customer_id, $email);

                    } else
                        //la cuenta pertenece a gandhi, regresa status 0, orbile_id de la cuenta primaria y correo de la cuenta primaria
                        if ($status_porrua == 1 && $status_gandhi == 1) {

                            return new CheckLoginResponse(0, $customer->orbile_customer_id, $email);

                        } else
                            //la cuenta pertenece a gandhi y porrua, regresa status 0, orbile_id de la cuenta primaria y correo de la cuenta primaria
                            if ($status_porrua == 0 && $status_gandhi == 1) {

                                return new CheckLoginResponse(0, $customer->orbile_customer_id, $email);

                            } else
                                //la cuenta no pertenece a ninguno de los partners, regresa status 1, orbile_id null y correo null
                                if ($status_porrua == 1 && $status_gandhi == 0) {

                                    return new CheckLoginResponse(1, null, null);
                                }

                }

                //Se revisan respuestas para mandar resultado
                //la cuenta es primaria
                else {
                    //la cuenta pertenece a porrua, regresa status 0, orbile_id  y correo
                    if ($status_porrua == 0 && $status_gandhi == 0) {

                        return new CheckLoginResponse(0, $customer_p->orbile_customer_id, $customer_p->email);

                    } else
                        //la cuenta pertenece a gandhi, regresa status 0, orbile_id  y correo
                        if ($status_porrua == 1 && $status_gandhi == 1) {

                            return new CheckLoginResponse(0, $customer_p->orbile_customer_id, $customer_p->email);

                        } else
                            //la cuenta pertenece a porrua y gandhi, regresa status 0, orbile_id  y correo
                            if ($status_porrua == 0 && $status_gandhi == 1) {

                                return new CheckLoginResponse(0, $customer_p->orbile_customer_id, $customer_p->email);

                            } else
                                //la cuenta nopertnece a los partners, regresa status 1, orbile_id y correo null
                                if ($status_porrua == 1 && $status_gandhi == 0) {

                                    return new CheckLoginResponse(1, null, null);
                                }
                }
            }

            /**
             * se cacha la excepcion cuando el user o passwd son incorrectos o no existe.
             */
        } catch (SoapFault $e) {

            $this->logger->log($e->getMessage());
            return new CheckLoginResponse(1, null,null);

        }


    }
}
