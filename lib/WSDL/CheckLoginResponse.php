<?php

class CheckLoginResponse
{

    /**
     * @var int $Status
     * @access public
     */
    public $Status = null;

    /**
     * @var string $UserRef
     * @access public
     */
    public $UserRef = null;

    /**
     * @var
     */
    public $Email = null;

    /**
     * @param int $Status
     * @param string $UserRef
     * @access public
     */
    public function __construct($Status, $UserRef, $Email)
    {
        $this->Status = $Status;
        $this->UserRef = $UserRef;
        $this->Email = $Email;
    }

}
